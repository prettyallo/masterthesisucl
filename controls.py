# flake8: noqa

# Controls for webapp



TEXT_APP = dict(
    fr_fr = dict(
        title_tab = "Analysing a data on OVH: cloud provider",
        title_filter_by_day = "Filter",
        row_selected = " {:,} ",
        hour_slider = "Time slider:",
        date_range = "Date: (JJ/MM/AAAA)",
        router_type= "Node type:",
        title_graph_figure= "Number of nodes over time",
        title_Upload_folder = "choose file",
        router_type_select = [
                                {'label': 'All ', 'value': 2},
                                {'label': 'Inter-domaine ', 'value': 1},
                                {'label': 'Extra-domaine ', 'value': 0}

                            ],
        type_of_link= "Type de lien:",
        bar_chart_timer_detail = [
                                {'label': 'Display hours', 'value': 1},
                                {'label': 'Hide hours', 'value': 0}
                            ],
        type_of_link_select  = [
                                {'label': 'All ', 'value': 'all'},
                                {'label': 'Lien montante', 'value': 0},
                                {'label': 'lien descendante', 'value': 1}
                            ],
        select_router = "Search node",
        dead_links = "Filter with Null links (flow = 0%)",
        dead_links_select = [
                                {'label': 'No preference', 'value': 'no_specific_null_flow'},
                                {'label': 'Yes ', 'value': 'null_flow'},
                                {'label': 'No ', 'value': 'no_null_flow'},
                                {'label': 'Choose number', 'value': 'select_null_flow_number'}
                            ],
        load_diff_links = "load difference on links",
        load_diff_links_select = [
                                {'label': 'No preference ', 'value': 'no_specific_diff_flow'},
                                {'label': 'With variation', 'value': 'diff_flow'},
                                {'label': 'No Variation', 'value': 'no_diff_flow'},
                                {'label': 'Choose number', 'value': 'select_null_flow_number'}],
        diff_peer = [
                                {'label': 'Yes ', 'value': 'YES'},
                                {'label': 'No ', 'value': 'NO'},
                                {'label': 'All', 'value': 'TOUS'}],
        metric_msg = "Select features",
        metric_select = [
                            {'label': 'Yes ', 'value': 'YES_DL'},
                            {'label': 'No ', 'value': 'NO_DL'},
                            {'label': 'Choose number', 'value': 'NUM_DL'}
                        ]
    ),

    en_us = dict( )
)


OPTION_APP = dict(
    fr_fr = dict(
        radio_expand = [
            {'label': 'selection Node with max load', 'value': 'followers'},
            {'label': 'Display all nodes that have the max load', 'value': 'following'},
                            ],

    list_column_stat_per_peer = ['Number_UPLINK_per_peer','Number_of_UPLINK_with_a_null_flow_per_peer','Number_of_UPLINK_with_low_flow_per_peer','UPLINK_flow_per_peer',
                                 'Number_DOWNLINK_per_peer','Number_of_DOWNLINK_with_a_null_flow_per_peer', 'Number_of_DOWNLINK_with_low_flow_per_peer','DOWNLINK_flow_per_peer']
    )
)

TEXT_PIE_GRAPH = dict(
    fr_fr = dict(
        title = "Node type",
        text = ["nœud Inter-domaine (int)", "nœud Extra-domaine (Ext)"],
    ),
    en_us = dict(

    )
)

