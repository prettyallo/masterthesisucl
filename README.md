# Dashboards de visualisation des donnees Weathermap OVH

Application web permettant de visualiser les données.

---

## Installation des dépendances  

1. Cloner le dépôt

```bash
git clone https://prettyallo@bitbucket.org/prettyallo/masterthesisucl.git
cd masterthesisucl
mkdir data
```

2. Installer les dépendances 
   
```bash
pip3 install -r requirements.txt
```

---

## Traitement du  yaml --> csv 

1. Dans  `src/createDataframe.py` changer le chemin du répertoire `directory` où se trouvent les fichiers yaml. et le `FileOutput` nom du fichier de sorti

```
directory = "/Path/to/folder/weathermap_MM-YYYY_yaml/"
FileOutput = "df_MM-YYYY"
output = "data/"+FileOutput+".csv"
building_metadata(directory, output)
print('****************** OK ******************', output)
```

2. Exécuter le fichier `src/createDataframe.py`.

```bash
python3 createDataframe.py
```
 Note : pour un mois de dataset yaml, ce processus met environ 45 min pour générer le fichier csv dans  `data/df_MM-YYYY.csv`

---

## Lancer l'application web

1. Dans `app.py` renseigner le non du fichier csv obtenue `df_MM-YYYY` dans la liste `list_of_name`  .
```
# Load data
list_of_name = ['df_MM-YYYY.csv','df2_MM-YYYY.csv']
```

2. L'application s'exécute sur l'ordinateur local et se lancer sur le terminal de la machine :
```bash
python3 app.py
```
3. Une exécution réussie, affiche les messages ci-dessous dans la fenêtre du terminal

```
Dash is running on http://127.0.0.1:8050/

 * Serving Flask app 'app' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
```

4. Sur un navigateur web, saisir l'URL `http://127.0.0.1:8050/`

---

## View

![alt tag](assets/DashboardsVisualisationOVH.png)