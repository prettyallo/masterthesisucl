from utils import *
import utils
from datetime import datetime
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import yaml
import csv

from networks import Node


def create_maxtrix(matrix_type, relation_dictionary, timestamp, csv_file):

    start_position = 2
    header_csv = list()
    data_matrix = dict()

    header_csv.append("timestamp")
    header_csv.append("node")

    if matrix_type == 1:

        for node, list_peer in relation_dictionary.items():
            data_matrix[node] = list()

            for peer, list_value in list_peer.items():

                for directional, arc in list_value.items():
                    header_csv.extend(arc)
    else:
        header_csv.extend(Node.list_nodes)
        for node, list_peer in relation_dictionary.items():
            data_matrix[node] = list()

    if Node.Check_header == timestamp:

        with open(csv_file, "w") as f:
            wr = csv.writer(f, delimiter=",")
            wr.writerow(header_csv)

        Node._header = header_csv

    selections = set(Node._header)
    indexes = [header_csv[ind] for ind, el in enumerate(header_csv) if el not in selections]

    if len(indexes) != 0:
        Node._header.extend(indexes)

        with open(csv_file) as f:
            lines = f.readlines()

        new_line = ""
        for i in Node._header:
            new_line = new_line + str(i) + ","

        new_line = new_line[:-1] + "\n"
        lines[0] = new_line

        with open(csv_file, "w") as f:
            f.writelines(lines)

    full_header = Node._header
    for node , value in data_matrix.items():
        data_matrix[node] = [0 for i in range(start_position,len(full_header))]

    for node, list_peer in relation_dictionary.items():

        for peer, list_value in list_peer.items():

            for directional, arc in list_value.items():

                if matrix_type == 1:

                    for value_arc in arc:

                        indexOf = full_header.index(value_arc)
                        if directional == -1 :
                            data_matrix[node][indexOf-start_position] = -1
                            data_matrix[peer][indexOf-start_position] = 1
                        if directional == 1:
                            data_matrix[node][indexOf-start_position] = 1
                            data_matrix[peer][indexOf-start_position] = -1

                else:
                    indexOfpeer = full_header.index(peer)
                    indexOfnode = full_header.index(node)

                    if directional == -1:
                        data_matrix[node][indexOfpeer - start_position] = (sum(arc)/len(arc))
                    if directional == 1:
                        data_matrix[peer][indexOfnode - start_position] = (sum(arc)/len(arc))

    for node, arc_value in data_matrix.items():
        instances = list()
        instances.append(timestamp)
        instances.append(node)
        instances.extend(arc_value)

        with open(csv_file, "a") as f:
            wr = csv.writer(f, delimiter=",")
            wr.writerow(instances)



def add_to_dataFrame(dictionary, timestamp, csv_file):
    '''
    Enregistre chaque instance déjà traiter (ligne) dans le fichier csv.

    Parameters
    ----------
    dictionary  : est un Dictionnaire , donc la clé est le nom du nœud et la valeur est une liste de liste : [ [Value0, Value1, Value2, Value3, Value4, Value5], ]
    timestamp   : le timestamp qui se trouve sur le nom du fichier
    csv_file    : le fichier.cvs ou serons enregistrer les instance extraite depuis le dictionnaire.

    Returns
    -------
    Ne retourne Aucune valeur.

    '''


    for node, links in dictionary.items():
        node_object = Node(dictionary, node, links, timestamp)
        node_object.get_UPLINK_value()
        node_object.get_DOWNLINK_value()
        instances = list()

        #if (node_object.get_UPLINK_load() != None) and (node_object.get_DOWNLINK_load() != None):
        instances.append(timestamp)  # 0
        instances.append(node)  # 1
        instances.append(node_object.Node_type)

        instances.append(node_object.Conservation_of_Flows)
        instances.append(node_object.Sum_of_the_flows_entering_in_node)
        instances.append(node_object.Sum_of_the_flows_exiting_in_node)
        instances.append(node_object.Number_of_neighbours)
        instances.append(node_object.Neighbor_list)
        instances.append(node_object.Two_way_flow)

        instances.append(node_object.Total_number_of_UPLINK)
        instances.append(node_object.Number_UPLINK_per_peer)
        instances.append(node_object.Total_number_of_UPLINK_with_a_null_flow)
        instances.append(node_object.Number_of_UPLINK_with_a_null_flow_per_peer)
        instances.append(node_object.Total_number_of_UPLINK_with_a_low_flow)
        instances.append(node_object.Number_of_UPLINK_with_low_flow_per_peer)
        instances.append(node_object.UPLINK_flow_per_peer)
        instances.append(node_object.UPLINK_label_per_peer)
        instances.append(node_object.max_flow_difference_in_UPLINK)
        instances.append(node_object.min_flow_difference_in_UPLINK)
        instances.append(node_object.max_UPLINK_in_peer)
        instances.append(node_object.min_UPLINK_in_peer)
        instances.append(node_object.link_difference_in_UPLINK)

        instances.append(node_object.Total_number_of_DOWNLINK)
        instances.append(node_object.Number_DOWNLINK_per_peer)
        instances.append(node_object.Total_number_of_DOWNLINK_with_a_null_flow)
        instances.append(node_object.Number_of_DOWNLINK_with_a_null_flow_per_peer)
        instances.append(node_object.Total_number_of_DOWNLINK_with_a_low_flow)
        instances.append(node_object.Number_of_DOWNLINK_with_low_flow_per_peer)
        instances.append(node_object.DOWNLINK_flow_per_peer)
        instances.append(node_object.DOWNLINK_label_per_peer)
        instances.append(node_object.max_flow_difference_in_DOWNLINK)
        instances.append(node_object.min_flow_difference_in_DOWNLINK)
        instances.append(node_object.max_DOWNLINK_in_peer)
        instances.append(node_object.min_DOWNLINK_in_peer)
        instances.append(node_object.link_difference_in_DOWNLINK)


        with open(csv_file, "a") as f:
            wr = csv.writer(f, delimiter=",")
            wr.writerow(instances)

    # csv_file = "data/Incidence_matrix.csv"
    # create_maxtrix(1,Node.directed_graph, timestamp, csv_file)
    # Node.directed_graph = dict()
    # Node.Number_of_arc = 0

    # csv_file = "data/Adjacency_matrix.csv"
    # create_maxtrix(2, Node.relation_adjacency_matrix, timestamp, csv_file)
    # Node.relation_adjacency_matrix = dict()




def median_same_link(links):
    '''
    Permet de l’ensemble des connexion a un même voisin en un liens unique. Et Pour chaque liens unique, il calcule le nombre de lien de connexion avec le voisin, le nombre De liens mort, le nombre de liens BGP et la charge médiane .
    Pour cela il Parcoure une liste de liste. Et compare l’élément a la potion N*2 de chaque sous liste. Cet élément est le ‘peer’ ( le nom du voisin auquel le neud a une connexion). Il regroupe en une seul sous-liste toutes les sous-liste qui on le même nom de peer, puis calcule  utilise l’élément a la position N*1 pour calculer la valeur médiane.

    REMARQUE : le calcul de la médiane prend uniquement en compte les valeur de charge >= 2. Les valeur inferieur ne sont pas considérer comme du trafic utiles.  Les valeur 1 et 2 sont considérer comme des annonce BGP sur le liens. Et les valeur nul (0) sont considérer comme des liens mort.

    Parameters
    ----------
    links   : est une liste de liste  structurer comme ceci [ ['#4', '1', 'vie-vie2-pb1-nc5'], ['#3', '5', 'ams-1-n7'], ..., [] ]

    Returns
    -------
    une liste de liste structurer comme ceci : [ [Value0, Value1, Value2, Value3, Value4, Value5], [Value0, Value1, Value2, Value3, Value4, Value5], ..., [] ]

    Avec
    Value0 = nombre de connexion avec le même voisins (peer)
    Value1 = valeur médiane du total des charge des connexion du même voisin (peer)
    Value2 = Nom du voisin (peer)
    Value3 = Nombre de liens mort (link[1]=0)
    Value4 = Nombre connexion d’annonce BGP
    Value5 = Différence de charge : est obtenue avec la valeur max -min entre les liens ayant le même nom de voisin.

    '''

    result = list()
    number_of_links_with_neighbor = dict()
    number_of_dead_links = dict()
    all_peers = dict() # PEER {peer:[[list_load],[list_annonce]] , peer2:[[list_load],[list_annonce]] }

    for link in links:

        if (link[1] != None) and (link[2] != None):

            if link[2] in all_peers:
                if link[1] >= 2:
                    all_peers[link[2]][0].append(link[1])
                else:
                    all_peers[link[2]][1].append(link[1])
                    if link[1] == 0:
                        number_of_dead_links[link[2]] += 1

                number_of_links_with_neighbor[link[2]] += 1

            else:
                all_peers[link[2]] = list()
                all_peers[link[2]].append(list())
                all_peers[link[2]].append(list())

                if link[1] >= 2:
                    all_peers[link[2]][0].append(link[1])
                else:
                    all_peers[link[2]][1].append(link[1])
                    if link[1] == 0:
                        number_of_dead_links[link[2]] = 1

                number_of_links_with_neighbor[link[2]] = 1
                number_of_dead_links[link[2]] = 0

    for peer, list_load_and_annouce in all_peers.items():
        if len(list_load_and_annouce[0])==0:
            load_charge = abs(max(list_load_and_annouce[1]) - min(list_load_and_annouce[1]))
            result.append(list(
                [number_of_links_with_neighbor[peer], statistics.median(list_load_and_annouce[1]), peer,
                 number_of_dead_links[peer], list_load_and_annouce[1].count(1), load_charge]))
        else:
            load_charge = abs(max(list_load_and_annouce[0]) - min(list_load_and_annouce[0]))
            result.append(list([number_of_links_with_neighbor[peer], statistics.median(list_load_and_annouce[0]), peer, number_of_dead_links[peer], list_load_and_annouce[1].count(1), load_charge ]))

    return result


def explore_yaml_file(yaml_file, timestamp, csv_file):
    '''
    Permet d’explorer le contenue d’un fichier YAML, et de le sauvegarder dans un fichier csv en fonction de la structure des colonnes de la variable ‘columns’.
    Le paramètre timestamp est utiliser pour renseigne la colonne ‘date’.
    Pour chaque nœud, on liste l’ensemble de toutes les liens connecter. Puis on ajoute ce nœud dans un dictionnaire, donc la clé est le nom du nœud et la valeur est une liste de liste. Chaque élément de la premier liste représente l’ensemble des liens connecter sur le nœud,  et chaque élément de la deuxième liste représente les caractéristiques du lien.
    Cette fonction Fait également appel à la fonction median_same_link() pour faire permettre de regrouper tous les liens d’un appartenant au même nœud voisin.

    Parameters
    ----------
    yaml_file   : le fichier.yml
    timestamp   : le timestamp qui se trouve sur le nom du fichier
    csv_file    : le fichier.cvs ou serons enregistrer les information extraite depuis le fichier.yml

    Returns
    -------
    Un Dictionnaire , donc la clé est le nom du nœud et la valeur est une liste de liste : [ [Value0, Value1, Value2, Value3, Value4, Value5], ]
    Ex :
    {'1AND1' : [ ['7', '14', 'vie-vie2-pb1-nc5', '1', '2', '13'], ['7', '14', 'ams-1-n7', '0', '2', '10'], ..., [] ]
     'A1TEL' : [ ['7', '14', 'par-th2-pb3-nc5', '0', '2', '9'], ['7', '14', 'fra-1-n7', '1', '1', '11'], ..., [] ]
     'AMAZON' : [ ['7', '14', 'ams-5-n7', '1', '2', '10'], ['7', '14', 'par-th2-pb3-nc3', '0', '0', '10'], ..., [] ]
    }

    '''

    dictionary = dict()

    with open(yaml_file, 'r') as file:
        try:
            yaml_content = yaml.load(file)

            for node, links in yaml_content.items():
                dictionary[node] = list()

                for link, contain_link in links.items():
                    # print(f"{sub_key1}: {sub_value1}")

                    for element_in_link in contain_link:
                        # print(f"{sub_value2}")
                        _links = list()

                        for key_element, value_element in element_in_link.items():
                            # print(f"{sub_value3}")
                            # à la fin de cette boucle la variable 'links' une liste d'élément comme ceci : ['#4', '1', 'vie-vie2-pb1-nc5']
                            _links.append(value_element)

                        # On prend toute la liste de la variable '_links' , qu'on ajoute dans une autre liste. Dans la variable 'dictionary', c'est un dictionnaire donc chaque clé a pour valeur une liste de liste.
                        dictionary[node].append(_links)

                # On utilise la median_same_link() pour effectue un traitement sur cette liste de liste. cela pour regrouper en un seul lien tous les liens qui on le même nom de voisin
                #dictionary[node] = median_same_link(dictionary[node])

            add_to_dataFrame(dictionary, timestamp, csv_file)


        except yaml.YAMLError as exc:
            print(exc)

    return dictionary


def building_metadata(src_dir, csv_file):
    '''
    Permet de générer un tableau CVS  a partir d’un ensemble de fichier YAM. Le nom des Colum du tableau CVS  est défini par la variable "colums".
    Le calcule et le traitement des valeurs du table CVS est effectuer par deux autre fonction : "explore_yaml_file()" et ""

    Parameters
    ----------
    src_dir     : Le chemin/répertoire ou se trouve l’ensemble des fichiers YAML
    csv_file    : Le fichier '.csv' Ou sera sauvegarder les information per-traiter

    Returns
    -------
    Ne retourne rien. Mais écrase et remplir le fichier '.csv' à chaque exécution.

    '''

    columns = ['Date', 'Node', 'Node_type', 'Conservation_of_Flows',
               'Sum_of_the_flows_entering_in_node', 'Sum_of_the_flows_exiting_in_node',
               'Number_of_neighbours', 'Neighbor_list', 'Two_way_flow',

               'Total_number_of_UPLINK', 'Number_UPLINK_per_peer',
               'Total_number_of_UPLINK_with_a_null_flow', 'Number_of_UPLINK_with_a_null_flow_per_peer',
               'Total_number_of_UPLINK_with_a_low_flow', 'Number_of_UPLINK_with_low_flow_per_peer',
               'UPLINK_flow_per_peer', 'UPLINK_label_per_peer', 'max_flow_difference_in_UPLINK', 'min_flow_difference_in_UPLINK',
               'max_UPLINK_in_peer', 'min_UPLINK_in_peer', 'link_difference_in_UPLINK',

               'Total_number_of_DOWNLINK', 'Number_DOWNLINK_per_peer',
               'Total_number_of_DOWNLINK_with_a_null_flow', 'Number_of_DOWNLINK_with_a_null_flow_per_peer',
               'Total_number_of_DOWNLINK_with_a_low_flow', 'Number_of_DOWNLINK_with_low_flow_per_peer',
               'DOWNLINK_flow_per_peer', 'DOWNLINK_label_per_peer', 'max_flow_difference_in_DOWNLINK', 'min_flow_difference_in_DOWNLINK',
               'max_DOWNLINK_in_peer', 'min_DOWNLINK_in_peer', 'link_difference_in_DOWNLINK'
               ]


    # Tant que le fichier est ouvert en écriture, on écrase tout son contenue et on écrit le contenu de la variable 'columns'
    with open(csv_file,"w") as f:
        wr = csv.writer(f, delimiter=",")
        wr.writerow(columns)

    # data :
    data = dict()
    # KEYWORD: Mot clé qui permet non seulement de prendre uniquement les fichier Yaml, mais en plus permet d’extraire le temps timestamp du nom du fichier.
    KEYWORD = "weathermap_"

    files = os.listdir(src_dir)
    # if not os.path.exists(dstdir): # dest path doesnot exist
    #    os.makedirs(dstdir)
    total_files = coout_file(src_dir)

    # Pour chaque fichier YAML, on extrait le timestamp de son nom, puis on passer le chemin de ce fichier a la fonction explore_yaml_file() pour extraire et traiter. Les information contenue dans le fichier
    for i, file in enumerate(files):
        if str(file).find("yaml") != -1 & str(file).find(KEYWORD) != -1:

            Timer = str(file).replace(KEYWORD, "").replace(".yaml", "")
            print("TRAITEMENT ", i + 1, "/", total_files," | TIMER =", Timer)
            data[int(Timer)]= explore_yaml_file(os.path.join(src_dir, file), int(Timer), csv_file)




def Pre_process_data(df, col):
    '''
    Input: Data-frame and Column name.
    Operation: Fills the nan values with the minimum value in their respective column.
    Output: Returns the pre-processed data-frame.
    '''
    # df['primary_use'] = df['primary_use'].astype("category").cat.codes
    print("Name of column with NaN: " + str(col))
    print(df[col].value_counts(dropna=False, normalize=True).head())
    df[col].replace(np.inf, -1, inplace=True)

    return df

def reduce_mem_usage(df):
    '''
    Input - data-frame.
    Operation - Reduce memory usage of the data-frame.
    '''
    start_mem_usg = df.memory_usage().sum() / 1024 ** 2
    print("Memory usage of properties dataframe is :", start_mem_usg, " MB")
    # NAlist = [] # Keeps track of columns that have missing values filled in.
    for col in df.columns:
        if df[col].dtype != object:  # Exclude strings
            # Print current column type
            print("******************************")
            print("Column: ", col)
            print("dtype before: ", df[col].dtype)
            # make variables for Int, max and min
            IsInt = False
            mx = df[col].max()
            mn = df[col].min()
            # print("min for this col: ",mn)
            # print("max for this col: ",mx)
            # Integer does not support NA, therefore, NA needs to be filled
            if not np.isfinite(df[col]).all():
                # NAlist.append(col)
                df = Pre_process_data(df, col)

            # test if column can be converted to an integer
            asint = df[col].fillna(0).astype(np.int64)
            result = (df[col] - asint)
            result = result.sum()
            if result > -0.01 and result < 0.01:
                IsInt = True
                # Make Integer/unsigned Integer datatypes
            if IsInt:
                if mn >= 0:
                    if mx < 255:
                        df[col] = df[col].astype(np.uint8)
                    elif mx < 65535:
                        df[col] = df[col].astype(np.uint16)
                    elif mx < 4294967295:
                        df[col] = df[col].astype(np.uint32)
                    else:
                        df[col] = df[col].astype(np.uint64)
                else:
                    if mn > np.iinfo(np.int8).min and mx < np.iinfo(np.int8).max:
                        df[col] = df[col].astype(np.int8)
                    elif mn > np.iinfo(np.int16).min and mx < np.iinfo(np.int16).max:
                        df[col] = df[col].astype(np.int16)
                    elif mn > np.iinfo(np.int32).min and mx < np.iinfo(np.int32).max:
                        df[col] = df[col].astype(np.int32)
                    elif mn > np.iinfo(np.int64).min and mx < np.iinfo(np.int64).max:
                        df[col] = df[col].astype(np.int64)
                        # Make float datatypes 32 bit
            else:
                df[col] = df[col].astype(np.float32)

            # Print new column type
            print("dtype after: ", df[col].dtype)
            print("******************************")
    # Print final result
    print("___MEMORY USAGE AFTER COMPLETION:___")
    mem_usg = df.memory_usage().sum() / 1024 ** 2
    print("Memory usage is: ", mem_usg, " MB")
    print("This is ", 100 * mem_usg / start_mem_usg, "% of the initial size")
    return df
