# importation librairies
import os
from processing import building_metadata

if __name__ == "__main__":

    directory = "/Path/to/folder/weathermap_MM-YYYY_yaml/"
    FileOutput = "df_MM-YYYY"
    output = "data/"+FileOutput+".csv"
    building_metadata(directory, output)
    print('****************** OK ******************', output)

