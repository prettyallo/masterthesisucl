import os
import pathlib
import statistics
from datetime import datetime

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

from matplotlib import cm
from scipy.spatial import distance
from pickle import load

import pickle
import time
import pandas as pd
from calendar import monthrange




def load_HDR_data():
    """Load the data for the HDR dataset.
    
    Returns
    -------
        - data: dictionary with fields X, country_names,
                indicator_names and indicator_descriptions
                
    """
    
    fd = open('hdr_data.dat', 'rb')

    hdr_data = load(fd)
    fd.close()
    
    return hdr_data
    

def RGB_color(color_id, nb_colors, cmap='jet'):
    """ Returns a RGB code for a given number of possible colors.
    
    Parameters
    ----------
        - color_id: id of the color (0 <= color_id < nb_colors)
        - nb_colors: total number of possible colors
        - cmap: name of color map (string, optional)
        
    Returns
    -------
        - rgb_code: a valid RGB code, i.e. a 3-uple of values in [0, 1]
    
    For a list of color maps, see http://matplotlib.org/examples/color/colormaps_reference.html
    
    """
    
    cmap = cm.get_cmap('jet')

    return cmap(float(color_id) / nb_colors)
    

def show_annotated_clustering(X, clustering, labels):
    """Displays a clustering where each instance is labelled.
    
    Parameters
    ----------
        - X: 2D coordinates of objects
        - clustering: assignment to clusters
        - labels: text labels of objects
    
    clustering is expected to contain cluster ids between 0 and
    nb_clusters - 1.  Each cluster must contain a least one object.
    
    """
    
    nb_clusters = np.max(clustering)+1
    n, d = X.shape
    
    for i in range(n):
        plt.text(X[i, 0], X[i, 1], labels[i], fontsize=3, bbox=dict(facecolor=RGB_color(clustering[i], nb_clusters), alpha=0.5))
    
    plt.xlim(np.min(X[:, 0])-1, np.max(X[:, 0])+1)
    plt.ylim(np.min(X[:, 1])-1, np.max(X[:, 1])+1)


def find_closest_instances_to_kmeans(X, model):
    """Returns the set of instances which are the closest to the cluster centers in a k-means.
     
     Parameters
     ----------
        - X: instances (which were used to train the k-means model for clustering)
        - model: k-means model for clustering (produced by sklearn.cluster.kmeans)

    Returns
    -------
        - closest_instances: set of closest instances to the cluster centers
        - closest_indices: indice of closest instances in X
        
    """
    
    centroids = model.cluster_centers_
    nb_clusters, d = centroids.shape
    
    closest_instances = np.zeros((nb_clusters, d), dtype=float)
    closest_indices = np.zeros((nb_clusters, ), dtype=int)
    for cluster_id in range(nb_clusters):
        closest_indices[cluster_id] = np.argmin(sp.spatial.distance.cdist(X, centroids[cluster_id, :].reshape((1, -1))))
        closest_instances[cluster_id, :] = X[closest_indices[cluster_id], :]
        
    return closest_instances, closest_indices


#########################################################################


# maxValue : prend en argument un Dictionnaire qui a pour valeur des nombres.
# permet de trouver la/les  cles avec la valeur la plus éléve. Et retoure un Dictionnaire avec  la/les {key:valeur} ayant la valeurs la plus eleve.
# NB : si il y a plusieur cle ayant la valeur Max (ex : Max=120), ca vas retourner toutes ses cles dans le Dictionnaire . {key1:120, key2:120,}

def maxValue(Dictionary):
    """
    permet de trouver la/les  cles avec la valeur la plus éléve.

    Parameters
    ----------
    Dictionary : Dictionnaire qui a pour valeur des nombres

    Returns
    -------
    un Dictionnaire avec  la/les {key:valeur} ayant la valeurs la plus eleve.
    NB : si il y a plusieur cle ayant la valeur Max (ex : Max=120), ca vas retourner toutes ses cles dans le Dictionnaire . {key1:120, key2:120,}

    """
    RESULT = list()
    #RESULT = dict()
    VALUES = list(Dictionary.values())
    KEYS = list(Dictionary.keys())

    MAX_VAL = max(VALUES)

    for i, val in enumerate(VALUES):
        if val == MAX_VAL:
            RESULT.append(KEYS[i])
            #RESULT[KEYS[i]] = Dictionary[KEYS[i]]

    return RESULT


def minValue(Dictionary):
    """
    permet de trouver la ou les  cle ayant la valeur la plus bas dans un dictionnaire.

    Parameters
    ----------
    Dictionary  : Dictionnaire donc les valeurs sont des nombres

    Returns
    -------
    un Dictionnaire avec  la/les {key:valeur} ayant la valeurs la plus bas.
    NB : si il y a plusieur cle ayant la valeur Min (ex : Max=120), ca vas retourner toutes ses cles dans le Dictionnaire . {key1:120, key2:120,}.

    """
    RESULT = list()
    #RESULT = dict()
    VALUES = list(Dictionary.values())
    KEYS = list(Dictionary.keys())

    MAX_VAL = min(VALUES)

    for i, val in enumerate(VALUES):
        if val == MAX_VAL:
            RESULT.append(KEYS[i])
            #RESULT[KEYS[i]] = Dictionary[KEYS[i]]

    return RESULT


def computeMediaLoad(DATA, NODE, PEER, DATE):

    for LINK in DATA[PEER]:

        if (LINK[2] == NODE) & (LINK[3] == DATE):
            return LINK




def serializationObject(object, output_filename):
    '''
    Permet de sérialiser un Object (une variable) en un fichier binaire qui pourras être stocker

    Parameters
    ----------
    object          : Object ou variable a sérialiser.
    output_filename : Non du fichier de sorti ou sera stocker l’Object sur la machine.

    Returns
    -------
    Ne retourne Rien

    '''
    outfile = open(output_filename, 'wb')
    # source, destination
    pickle.dump(object, outfile)
    outfile.close()

def unSerializationObject(intput_filename):
    '''
    Permet de Désérialiser un fichier en un Object(une variable)

    Parameters
    ----------
    intput_filename : Non du fichier d'entre Désérialiser.

    Returns
    -------
    Un object(ou une variable) donc le 'type' est celui de l'object Désérialiser.

    '''

    infile = open(intput_filename, 'rb')
    new_object = pickle.load(infile)
    infile.close()

    return new_object


def timestamp_printf (timestamp):
    dt_format = datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
    dt_object = datetime.strptime(dt_format, "%d-%m-%Y %H:%M:%S")
    #dt_object = datetime.fromtimestamp(timestamp).strftime('%d/%m/%Y')

    return dt_object

def coout_file(dir):
    '''
    Compte le nombre de fichiers contenue dans un repertoire.

    Parameters
    ----------
    dir : chemin (ou nom) du répertoire ou on veut compter les fichiers.

    Returns
    -------
    Le nombre de fichier dans ce repertoire.

    '''
    initial_count = 0
    for path in pathlib.Path(dir).iterdir():
        if path.is_file():
            initial_count += 1

    return initial_count


# def CreateDataFrame(DataFrame, startDay, endDay):
#     for i in range(startDay, endDay):
#         s = str(i) + "/09/2021 00:00:00"
#         timestamp_creat = time.mktime(datetime.datetime.strptime(s, "%d/%m/%Y %H:%M:%S").timetuple())
#         # print(timestamp_creat)
#
#         timestamp_select = list(DataFrame.loc[DataFrame['date'] >= timestamp_creat]['date'])[0]
#
#         df = DataFrame.loc[DataFrame['date'] == timestamp_select]
#
#         if i == 1:
#             Newdf = df
#         else:
#             Newdf = pd.concat([Newdf, df], ignore_index=True)
#
#     return Newdf


def CreateDataFrameX(df, start_date, end_date, hour_slider):
    '''
    Permet de créer un sous-DataFrame dans un DataFrame en fonction des paramètres du filtre

    Parameters
    ----------
    df          : DataFrame principale
    start_date  : Date de debut au format AAAA-MM-JJ
    end_date    : Date de fin au format AAAA-MM-JJ
    hour_slider : tableau qui contient deux elements de type int: le premier represente l'heure de depart et le secont l'heure de fin.

    Returns
    -------
    Un DataFrame

    '''
    global Newdf
    sart_timestamp = str(start_date) + " " + str(hour_slider[0]) + ":00:00"
    end_timestamp = str(end_date) + " " + str(hour_slider[1]) + ":59:59"

    sart_timestamp = datetime.strptime(sart_timestamp, "%Y-%m-%d %H:%M:%S")
    end_timestamp = datetime.strptime(end_timestamp, "%Y-%m-%d %H:%M:%S")

    # print("DATE DE DEBUT = ",sart_timestamp)
    # print("DATE DE DEBUT = ", end_timestamp)

    if (sart_timestamp.month == end_timestamp.month) and (sart_timestamp.year == end_timestamp.year):

        last_day = monthrange(sart_timestamp.year, sart_timestamp.month)[1]

        for day_ in range(sart_timestamp.day, last_day + 1):
            s = str(day_) + "/" + str(sart_timestamp.month) + "/" + str(sart_timestamp.year) + " " + str(
                hour_slider[0]) + ":00:00"
            timestamp_creat = time.mktime(datetime.strptime(s, "%d/%m/%Y %H:%M:%S").timetuple())
            print('TIMESTAMP =',timestamp_creat)

            list_of_timestamp_creat = list(df.loc[df['date'] >= timestamp_creat]['date'])
            if len(list_of_timestamp_creat) != 0:
                timestamp_select = list_of_timestamp_creat[0]

                dff = df.loc[df['date'] == timestamp_select]

                if day_ == 1:
                    Newdf = dff
                else:
                    Newdf = pd.concat([Newdf, dff], ignore_index=True)
    else:
        for month_ in range(sart_timestamp.month, end_timestamp.month + 1):

            last_day = monthrange(sart_timestamp.year, sart_timestamp.month)[1]

            for day_ in range(sart_timestamp.day, last_day + 1):
                s = str(day_) + "/" + str(month_) + "/" + str(sart_timestamp.year) + " " + str(hour_slider[0]) + ":00:00"
                timestamp_creat = time.mktime(datetime.strptime(s, "%d/%m/%Y %H:%M:%S").timetuple())
                # print(timestamp_creat)

                list_of_timestamp_creat = list(df.loc[df['Date'] >= timestamp_creat]['Date'])
                if len(list_of_timestamp_creat) != 0:
                    timestamp_select = list_of_timestamp_creat[0]

                    dff = df.loc[df['Date'] == timestamp_select]

                    if day_ == 1:
                        Newdf = dff
                    else:
                        Newdf = pd.concat([Newdf, dff], ignore_index=True)

    return Newdf


def CreateDataFrame(df, start_date, end_date, hour_slider, initialisation_df=0):
    Newdf = pd.DataFrame()
    start_date = str(start_date) + " " + str(hour_slider[0]) + ":00:00"
    end_date = str(end_date) + " " + str(hour_slider[1]) + ":59:59"
    start_date = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
    end_date = datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")

    Num_year = (end_date.year - start_date.year)

    if (start_date.year == end_date.year):

        for month_ in range(start_date.month, end_date.month + 1):

            total_day_of_month = monthrange(start_date.year, month_)[1]

            for day_ in range(1, total_day_of_month + 1):

                #for hour_ in range(hour_slider[0], hour_slider[1] + 1):

                formate_start_date = str(day_) + "/" + str(month_) + "/" + str(start_date.year) + " " + str(hour_slider[0]) + ":00:00"
                start_timestamp = time.mktime(datetime.strptime(formate_start_date, "%d/%m/%Y %H:%M:%S").timetuple())

                formate_end_date = str(day_) + "/" + str(month_) + "/" + str(start_date.year) + " " + str(hour_slider[1]) + ":00:00"
                end_timestamp = time.mktime(datetime.strptime(formate_end_date, "%d/%m/%Y %H:%M:%S").timetuple())

                dff = df.loc[(df['Date'] >= start_timestamp) & (df['Date'] <= end_timestamp)]

                countItem = (dff.shape)[0]

                if countItem != 0:

                    print('Filter by Date =', start_date.year, '/', month_, '/', day_, ' H=[', hour_slider[0], ',', hour_slider[1], ']| ITEMS =', countItem)

                    if initialisation_df == 0:
                        Newdf = dff
                        initialisation_df += 1
                    else:
                        Newdf = pd.concat([Newdf, dff], ignore_index=True)

        return Newdf

    elif Num_year == 1:

        start_d1 = str(start_date.year) + "-" + str(start_date.month) + "-" + str(start_date.day)
        end_d1 = str(start_date.year) + "-" + str(12) + "-" + str(31)
        dff1 = CreateDataFrame(df, start_d1, end_d1, hour_slider)

        start_d2 = str(end_date.year) + "-" + str(1) + "-" + str(1)
        end_d2 = str(end_date.year) + "-" + str(end_date.month) + "-" + str(end_date.day)
        dff2 = CreateDataFrame(df, start_d2, end_d2, hour_slider)

        Newdf = pd.concat([dff1, dff2], ignore_index=True)
        return Newdf

    else:

        for year_ in range(start_date.year, end_date.year + 1):
            if (year_ == start_date.year):
                start_d1 = str(year_) + "-" + str(start_date.month) + "-" + str(start_date.day)
                end_d1 = str(start_date.year) + "-" + str(12) + "-" + str(31)
                dff1 = CreateDataFrame(df, start_d1, end_d1, hour_slider)
                Newdf = pd.concat([Newdf, dff1], ignore_index=True)

            elif (year_ == end_date.year):
                start_d1 = str(year_) + "-" + str(1) + "-" + str(1)
                end_d1 = str(year_) + "-" + str(12) + "-" + str(31)
                dff2 = CreateDataFrame(df, start_d1, end_d1, hour_slider)
                Newdf = pd.concat([Newdf, dff2], ignore_index=True)

            else:
                start_d1 = str(year_) + "-" + str(start_date.month) + "-" + str(start_date.day)
                end_d1 = str(start_date.year) + "-" + str(12) + "-" + str(31)
                dff3 = CreateDataFrame(df, start_d1, end_d1, hour_slider)
                Newdf = pd.concat([Newdf, dff3], ignore_index=True)

        return Newdf



def CreateDataFrameFast(df, start_date, end_date, hour_slider, initialisation_df=0):
    start_date = str(start_date) + " " + str(hour_slider[0]) + ":00:00"
    end_date = str(end_date) + " " + str(hour_slider[1]) + ":59:59"
    start_date = datetime.strptime(start_date, "%Y-%m-%d %H:%M:%S")
    end_date = datetime.strptime(end_date, "%Y-%m-%d %H:%M:%S")

    start_timestamp = time.mktime(start_date.timetuple())
    end_timestamp = time.mktime(end_date.timetuple())
    dff = df.loc[(df['Date'] >= start_timestamp) & (df['Date'] <= end_timestamp)]

    return dff

