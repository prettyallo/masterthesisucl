import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from toolz import compose, pluck, groupby, valmap, first, unique, get, countby
import plotly.express as px
#from .utils import maxValue
import time
#import datetime
from datetime import datetime
import ast
import random
import statistics



class Patterns:
    graph_edit = 0
    def __init__(self, df):
        self.Data_OVH_events = pd.read_csv("/Users/prettyallo/Data/Master_1_Cybersecurite/Annee_2020_2021/MasterThesis/MasterThesisUCL/data/Data_OVH_events.csv")
        self.df = df









    def Service_degradation(self, critical_value = 10):
        df = self.df

        print("HOW TO CHANGE ....")


        Newdf = pd.DataFrame()
        node_list = sorted(dict(df['Node'].value_counts()).keys())
        node_have_event = {}


        constante_metric1 = 'Sum_of_the_flows_exiting_in_node'


        for node in node_list:

            dict_constante_metric1 = dict(df[df['Node'] == node][constante_metric1].value_counts())

            # sorted_dict_constante_metric1 = OrderedDict(sorted(dict_constante_metric1.items(), key=lambda x: x[1], reverse=True))

            median_metric1 = statistics.median(sorted(dict_constante_metric1.keys()))
            critical_threshold = round((critical_value * median_metric1) / 100)

            for keys, value in dict_constante_metric1.items():
                if (keys < critical_threshold) and (value > statistics.median(list(dict_constante_metric1.values()))) and \
                        (node.isupper() == False):
                    if node in node_have_event:
                        node_have_event[node].append(keys)
                    else:
                        node_have_event[node] = list()
                        node_have_event[node].append(keys)

        valide_node_have_event = sorted(node_have_event.keys())
        print("LISTE DES NODES A SELECET = \n", valide_node_have_event)

        list_des_valeur_retenue = {}
        incidence_date = []
        incidence_url = []
        list_nodes = []

        for node in valide_node_have_event:
            dff = df[
                df['Node'].isin([node])
                & df[constante_metric1].isin(node_have_event[node])
                ]
            Newdf = pd.concat([Newdf, dff], ignore_index=True)
            list_des_valeur_retenue[node] = list(Newdf[Newdf['Node'] == node]['Date'])

        Newdf = Newdf.sort_values(by=['Date'])

        print("TAILLE DU DATAFRAME = ", Newdf.shape)

        # if timer_detail == 1:
        #     def convert_timestamp (timestamp) :
        #         return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        # else:
        #     def convert_timestamp (timestamp) :
        #         return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        url_node = {}
        # date_ = '11/2021'

        for node, list_timestamp in list_des_valeur_retenue.items():
            keysWord1 = node[0:node.find("-")]

            list_month = []
            url_node[node] = list()

            for timestamp in list_timestamp:

                month = datetime.fromtimestamp(timestamp).month
                year = datetime.fromtimestamp(timestamp).year
                date_ = str(month) + '/' + str(year)

                if (date_ in list_month) == False:
                    list_month.append(date_)

                    for index, row in self.Data_OVH_events.iterrows():

                        if ((row['name'].find(keysWord1) != -1) or ((row['name'].capitalize()).find(keysWord1) != -1) or (
                                (row['name'].upper()).find(keysWord1) != -1)) and (
                                str(row['timesPubli-planing']).find(date_) != -1):
                            url_node[node].append(row['code'])
                            incidence_date.append(datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S'))
                            incidence_url.append(row['code'])
                            list_nodes.append(node)

        return list_nodes, incidence_date, incidence_url, Newdf


    def upgrade_linecard(self):

        df = self.df

        print("HOW TO CHANGE ....")
        timer_detail = 1

        Newdf = pd.DataFrame()
        node_list = sorted(dict(df['Node'].value_counts()).keys())
        node_have_null_flow = {}

        metric1 = "Total_number_of_UPLINK_with_a_null_flow."
        constante_metric1 = 'Sum_of_the_flows_exiting_in_node'
        constante_metric2 = 'Total_number_of_UPLINK_with_a_low_flow'
        # constante_metric4 = 'Number_of_neighbours'
        # constante_metric5 = 'max_UPLINK_in_peer'


        for node in node_list:
            dict_number_of_links_a_null_flow = dict(df[df['Node'] == node][metric1].value_counts())
            dict_constante_metric1 = dict(df[df['Node'] == node][constante_metric1].value_counts())
            dict_constante_metric2 = dict(df[df['Node'] == node][constante_metric2].value_counts())
            # dict_constante_metric4 = dict(df[df['Node'] == node][constante_metric4].value_counts())
            # dict_constante_metric5 = dict(df[df['Node'] == node][constante_metric5].value_counts())


            lenVal1 = len(sorted(dict_constante_metric1.keys()))
            lenVal2 = len(sorted(dict_constante_metric2.keys()))
            # lenVal4 = len(sorted(dict_constante_metric4.keys()))
            # lenVal5 = len(sorted(dict_constante_metric5.keys()))


            #if len(sorted(dict_number_of_links_a_null_flow.keys())) != 1:  # ce nod a des liens mort
            if ((lenVal1 == 1) and ((lenVal2 == 1) or (lenVal2 == 2))  and (lenVal4 == 1) and (lenVal5 == 1)):  # ce nod a des liens mort
                node_have_null_flow[node] = list()
                #node_have_null_flow[node] = sorted(dict_number_of_links_a_null_flow.keys())
                list_of_number_links_a_null_flow = list(df[df['Node'] == node][metric1])
                median_null_flow = statistics.median(list_of_number_links_a_null_flow)
                max_null_flow = max(list_of_number_links_a_null_flow)

                marge1 = round((5*(max_null_flow - median_null_flow))/6)


                for i in sorted(dict_number_of_links_a_null_flow.keys()):
                    if i > median_null_flow+marge1:
                        node_have_null_flow[node].append(i)


        valide_node_null_flow = sorted(node_have_null_flow.keys())
        set1 = set(valide_node_null_flow)
        intersection = list(set1)

        print("LISTE DES NODES A SELECET = \n", intersection)


        for node in intersection:
            dff = df[
                df['Node'].isin([node])
                & df[metric1].isin(node_have_null_flow[node])
                #& self.df[metric2].isin(node_have_low_flow[node])
                ]
            Newdf = pd.concat([Newdf, dff], ignore_index=True)

        Newdf = Newdf.sort_values(by=['Date'])

        print("TAILLE DU DATAFRAME = ", Newdf.shape)

        # if timer_detail == 1:
        #     def convert_timestamp (timestamp) :
        #         return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        # else:
        #     def convert_timestamp (timestamp) :
        #         return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        #index_Newdf = sorted(Newdf.index)
        list_des_valeur_retenue = {}
        for node in intersection:
            empty_df = pd.DataFrame()
            dff = Newdf[Newdf['Node'] == node]
            dff = pd.concat([dff, empty_df], ignore_index=True)

            list_des_valeur_retenue[node] = list(dff[dff['Node'] == node]['Date'])



        incidence_date = []
        incidence_url = []
        list_nodes = []
        url_node = {}

        for node, list_timestamp in list_des_valeur_retenue.items():

            for timestamp in list_timestamp :
                incidence_date.append( convert_timestamp(timestamp))
                list_nodes.append(node)

            list_month = []
            url_node[node] = list()

            for timestamp in list_timestamp:

                month = datetime.fromtimestamp(timestamp).month
                year = datetime.fromtimestamp(timestamp).year
                date_ = str(month) + '/' + str(year)

                if (date_ in list_month) == False:
                    list_month.append(date_)

                    for index, row in self.Data_OVH_events.iterrows():

                        if ((row['name'].find(keysWord1) != -1) or (
                                (row['name'].capitalize()).find(keysWord1) != -1) or (
                                    (row['name'].upper()).find(keysWord1) != -1)) and (
                                str(row['timesPubli-planing']).find(date_) != -1):
                            url_node[node].append(row['code'])
                            incidence_date.append(datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S'))
                            incidence_url.append(row['code'])
                            list_nodes.append(node)



        return list_nodes, incidence_date, incidence_url, Newdf



    def da_all_incidence2(self):

        print("HOW TO CHANGE ....")
        df = self.df



        Newdf = pd.DataFrame()
        node_list = sorted(dict(df['Node'].value_counts()).keys())
        node_have_null_flow = {}
        node_have_max_diff = {}

        node_have_low_flow = {}

        metric1 = "Total_number_of_UPLINK_with_a_null_flow"
        metric2 = "Total_number_of_UPLINK_with_a_low_flow "

        for node in node_list:
            dict_number_of_links_a_null_flow = dict(
                df[df['Node'] == node][metric1].value_counts())
            dict_number_of_links_a_low_flow = dict(
                df[df['Node'] == node][metric2].value_counts())

            if len(sorted(dict_number_of_links_a_null_flow.keys())) != 1:  # ce nod a des liens mort
                node_have_null_flow[node] = list()
                #node_have_null_flow[node] = sorted(dict_number_of_links_a_null_flow.keys())
                list_of_number_links_a_null_flow = list(df[df['Node'] == node][metric1])
                median_null_flow = statistics.median(list_of_number_links_a_null_flow)

                for i in sorted(dict_number_of_links_a_null_flow.keys()):
                    if i > median_null_flow+1:
                        node_have_null_flow[node].append(i)

                #print("LISTE DES VALEURS node_have_null_flow = \n", node_have_null_flow[node])


            if len(sorted(dict_number_of_links_a_low_flow.keys())) != 1:  # ce nod a des liens mort
                node_have_low_flow[node] = list()
                #node_have_low_flow[node] = sorted(dict_number_of_links_a_low_flow.keys())
                list_of_number_links_a_low_flow = list(df[df['Node'] == node][metric2])
                median_low_flow = statistics.median(list_of_number_links_a_low_flow)

                for i in sorted(dict_number_of_links_a_null_flow.keys()):
                    if i > median_low_flow + 7:
                        node_have_low_flow[node].append(i)

                #print("LISTE DES VALEURS node_have_low_flow = \n", node_have_low_flow[node])



        valide_node_null_flow = sorted(node_have_null_flow.keys())
        valide_node_low_flow = sorted(node_have_low_flow.keys())

        set1 = set(valide_node_null_flow)
        set2 = set(valide_node_low_flow)
        intersection = list(set1 & set2)

        print("LISTE DES NODES A SELECET = \n", intersection)


        for node in intersection:
            dff = df[
                df['Node'].isin([node])
                & df[metric1].isin(node_have_null_flow[node])
                #& self.df[metric2].isin(node_have_low_flow[node])
                ]
            Newdf = pd.concat([Newdf, dff], ignore_index=True)

        Newdf = Newdf.sort_values(by=['Date'])

        print("TAILLE DU DATAFRAME = ", Newdf.shape)

        # if timer_detail == 1:
        #     def convert_timestamp (timestamp) :
        #         return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        # else:
        #     def convert_timestamp (timestamp) :
        #         return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        #index_Newdf = sorted(Newdf.index)
        list_des_valeur_retenue = {}
        for node in intersection:
            empty_df = pd.DataFrame()
            dff = Newdf[Newdf['Node'] == node]
            dff = pd.concat([dff, empty_df], ignore_index=True)

            list_des_valeur_retenue[node] = list(dff[dff['Node'] == node]['Date'])



        incidence_date = []
        incidence_url = []
        list_nodes = []
        url_node = {}

        for node, list_timestamp in list_des_valeur_retenue.items():

            for timestamp in list_timestamp :
                incidence_date.append( convert_timestamp(timestamp))
                list_nodes.append(node)

            list_month = []
            url_node[node] = list()

            for timestamp in list_timestamp:

                month = datetime.fromtimestamp(timestamp).month
                year = datetime.fromtimestamp(timestamp).year
                date_ = str(month) + '/' + str(year)

                if (date_ in list_month) == False:
                    list_month.append(date_)

                    for index, row in self.Data_OVH_events.iterrows():

                        if ((row['name'].find(keysWord1) != -1) or (
                                (row['name'].capitalize()).find(keysWord1) != -1) or (
                                    (row['name'].upper()).find(keysWord1) != -1)) and (
                                str(row['timesPubli-planing']).find(date_) != -1):
                            url_node[node].append(row['code'])
                            incidence_date.append(datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S'))
                            incidence_url.append(row['code'])
                            list_nodes.append(node)



        return list_nodes, incidence_date, incidence_url, Newdf


    def da_all_incidence(self):

        df = self.df

        value_fig = {}




        Newdf = pd.DataFrame()
        node_list = sorted(dict(df['Node'].value_counts()).keys())
        node_have_null_flow = {}
        node_have_not_null_flow = {}

        node_have_low_flow = {}
        node_have_not_low_flow = {}

        for node in node_list:
            dict_number_of_links_a_null_flow = dict(
                df[df['Node'] == node]['Total_number_of_UPLINK_with_a_null_flow'].value_counts())
            dict_number_of_links_a_low_flow = dict(
                df[df['Node'] == node]['Total_number_of_UPLINK_with_a_low_flow'].value_counts())

            if len(sorted(dict_number_of_links_a_null_flow.keys())) != 1:  # ce nod a des liens mort
                node_have_null_flow[node] = sorted(dict_number_of_links_a_null_flow.keys())
            else:
                node_have_not_null_flow[node] = sorted(dict_number_of_links_a_null_flow.keys())

            if len(sorted(dict_number_of_links_a_low_flow.keys())) != 1:  # ce nod a des liens mort
                node_have_low_flow[node] = sorted(dict_number_of_links_a_low_flow.keys())
            else:
                node_have_not_low_flow[node] = sorted(dict_number_of_links_a_low_flow.keys())


        valide_node_null_flow = sorted(node_have_null_flow.keys())
        valide_node_low_flow = sorted(node_have_low_flow.keys())

        set1 = set(valide_node_null_flow)
        set2 = set(valide_node_low_flow)
        intersection = list(set1 & set2)

        for node in intersection:
            dff = df[
                df['Node'].isin([node])
                & df['Total_number_of_UPLINK_with_a_null_flow'].isin(node_have_null_flow[node])
                & df['Total_number_of_UPLINK_with_a_low_flow '].isin(node_have_low_flow[node])
                ]
            Newdf = pd.concat([Newdf, dff], ignore_index=True)

        Newdf = Newdf.sort_values(by=['Date'])


        #index_Newdf = sorted(Newdf.index)
        list_des_valeur_retenue = {}
        for node in intersection:
            empty_df = pd.DataFrame()
            dff = Newdf[Newdf['Node'] == node]
            dff = pd.concat([dff, empty_df], ignore_index=True)

            value_fig[node] = list()
            XX = []
            YY= []
            list_des_valeur_retenue[node] = []
            index_dff= sorted(dff.index)

            for index, row in dff.iterrows():
                print('index =', index)

                if row['Total_number_of_UPLINK_with_a_null_flow'] != 0: # si le nombre de lien mort est quitter de 0 a une valeur superier a 0
                    liste_des_suivant = []  # on creer une liste dans laquel on va mettre toute les valeur des date qui suive

                    for i in range(index, index+12 ):
                        if i in index_dff:
                            liste_des_suivant.append(dff.iloc[i]['Total_number_of_UPLINK_with_a_null_flow'])

                    if max(liste_des_suivant) != 0 or max(liste_des_suivant) != None:

                        list_des_valeur_retenue[node].append(row['Date'])

                YY.append(row['Total_number_of_UPLINK_with_a_null_flow'])
                XX.append( convert_timestamp(row['Date']) )

        print( ' Nouvelle Taille = ',len(list(list_des_valeur_retenue.keys())),'| ',list(list_des_valeur_retenue.keys()) )
        #print( ' Liste des valueur Retenu  = ',list_des_valeur_retenue )

        incidence_date = []
        incidence_url = []
        list_nodes = []

        url_node = {}

        for node, list_timestamp in list_des_valeur_retenue.items():

            keysWord1 = node[0:node.find("-")]

            list_month = []
            url_node[node] = list()

            for timestamp in list_timestamp :

                month = datetime.fromtimestamp(timestamp).month
                year = datetime.fromtimestamp(timestamp).year
                date_ = str(month) + '/' + str(year)

                if (date_ in list_month) == False:
                    list_month.append(date_)

                    for index, row in self.Data_OVH_events.iterrows():

                        if ((row['name'].find(keysWord1) != -1) or (
                                (row['name'].capitalize()).find(keysWord1) != -1) or (
                                    (row['name'].upper()).find(keysWord1) != -1)) and (
                                str(row['timesPubli-planing']).find(date_) != -1):
                            url_node[node].append(row['code'])
                            incidence_date.append(datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S'))
                            incidence_url.append(row['code'])
                            list_nodes.append(node)


        return list_nodes, incidence_date, incidence_url, Newdf