from utils import *

#from src.utils import maxValue, minValue


class Node:

    node_number = 0
    def __init__(self, dictionary, name, links,  timestamp):

        Node.node_number += 1

        self.dictionary = dictionary
        self.name = name
        self.links = links
        self.timestamp = timestamp

        self.nbr_tx_neighbor_up_max = -1
        self.nbr_tx_neighbor_up_mix = -1

        if name.isupper():
            self.Node_type = 0
        else:
            self.Node_type = 1

        self.Conservation_of_Flows = 0
        self.Sum_of_the_flows_entering_in_node = 0
        self.Sum_of_the_flows_exiting_in_node = 0
        self.Number_of_neighbours = 0
        self.Neighbor_list = []
        self.Two_way_flow = []

        self.Total_number_of_UPLINK = 0
        self.Number_UPLINK_per_peer = []
        self.Total_number_of_UPLINK_with_a_null_flow = 0
        self.Number_of_UPLINK_with_a_null_flow_per_peer = []
        self.Total_number_of_UPLINK_with_a_low_flow = 0
        self.Number_of_UPLINK_with_low_flow_per_peer = []
        self.UPLINK_flow_per_peer = []
        self.UPLINK_label_per_peer = []

        self.link_difference_in_UPLINK = 0
        self.max_flow_difference_in_UPLINK = 0
        self.max_UPLINK_in_peer = 0
        self.min_flow_difference_in_UPLINK = 0
        self.min_UPLINK_in_peer = 0

        self.Total_number_of_DOWNLINK = 0
        self.Number_DOWNLINK_per_peer = []
        self.Total_number_of_DOWNLINK_with_a_null_flow = 0
        self.Number_of_DOWNLINK_with_a_null_flow_per_peer = []
        self.Total_number_of_DOWNLINK_with_a_low_flow = 0
        self.Number_of_DOWNLINK_with_low_flow_per_peer = []
        self.DOWNLINK_flow_per_peer = []
        self.DOWNLINK_label_per_peer = []

        self.link_difference_in_DOWNLINK = 0
        self.max_flow_difference_in_DOWNLINK = 0
        self.max_DOWNLINK_in_peer = 0
        self.min_flow_difference_in_DOWNLINK = 0
        self.min_DOWNLINK_in_peer = 0



    def get_name(self):
        return self.name

    def get_link(self, index=-1):
        if index == -1 :
            return self.links
        else:
            return self.links[index]

    def set_link(self, link):
        self.links.append(link)




    ################################################
    def get_UPLINK_value(self):

        if Node.node_number == 1:
            Node.Check_header = self.timestamp

        Number_UPLINK_per_peer = dict()
        Number_of_UPLINK_with_a_null_flow_per_peer = dict()
        Number_of_UPLINK_with_low_flow_per_peer = dict()
        all_peers = dict()  # PEER {peer:[load1, load2, load3] , peer2:[load1, load2, load3] }
        all_label_per_peers = dict()  # PEER {peer:[#1, #2, #3] , peer2:[#1, #2, #3] }
        flow_difference_per_peer_UPLINK = []


        Directional = -1
        for link in self.links:

            if (link[1] != None) and (link[2] != None):

                # Node.Number_of_arc += 1 # Utiliser pour la matrix d'incidance.
                # arc = str(self.list_nodes.index(self.name))+"_"+str(self.list_nodes.index(link[2]))+"_"+str(Node.Number_of_arc) + str(link[0]) # Utiliser pour la matrix d'incidance.

                if link[2] in all_peers:
                    all_peers[link[2]].append(link[1])
                    all_label_per_peers[link[2]].append(link[0])

                    if link[1] <= 2:

                        if link[1] == 0:
                            Number_of_UPLINK_with_a_null_flow_per_peer[link[2]] += 1
                        else:
                            Number_of_UPLINK_with_low_flow_per_peer[link[2]] += 1

                    Number_UPLINK_per_peer[link[2]] += 1

                else:
                    all_peers[link[2]] = list()
                    all_label_per_peers[link[2]] = list()
                    all_peers[link[2]].append(link[1])
                    all_label_per_peers[link[2]].append(link[0])
                    Number_of_UPLINK_with_a_null_flow_per_peer[link[2]] = 0
                    Number_of_UPLINK_with_low_flow_per_peer[link[2]] = 0

                    if link[1] <= 2:

                        if link[1] == 0:
                            Number_of_UPLINK_with_a_null_flow_per_peer[link[2]] = 1
                        else:
                            Number_of_UPLINK_with_low_flow_per_peer[link[2]] = 1

                    Number_UPLINK_per_peer[link[2]] = 1

        for peer, list_flows in all_peers.items():

            self.Neighbor_list.append(peer)
            self.Two_way_flow.append(0) # initialise toute les liens comme etant uni-directionnel avec ses voisins.
            self.Total_number_of_UPLINK += len(list_flows)
            self.Total_number_of_UPLINK_with_a_null_flow += Number_of_UPLINK_with_a_null_flow_per_peer[peer]
            self.Number_of_UPLINK_with_a_null_flow_per_peer.append(Number_of_UPLINK_with_a_null_flow_per_peer[peer])
            self.Number_of_UPLINK_with_low_flow_per_peer.append(Number_of_UPLINK_with_low_flow_per_peer[peer])
            self.Total_number_of_UPLINK_with_a_low_flow += Number_of_UPLINK_with_low_flow_per_peer[peer]
            self.Number_UPLINK_per_peer.append(Number_UPLINK_per_peer[peer])
            self.UPLINK_flow_per_peer.append(list_flows)
            self.UPLINK_label_per_peer.append(all_label_per_peers[peer])
            self.Sum_of_the_flows_exiting_in_node += sum(list_flows)
            flow_difference_per_peer_UPLINK.append(abs(max(list_flows) - min(list_flows)))

        self.Number_of_neighbours = len(list(all_peers.keys()))

        if len(flow_difference_per_peer_UPLINK) != 0:
            self.max_flow_difference_in_UPLINK = max(flow_difference_per_peer_UPLINK)
            self.min_flow_difference_in_UPLINK = min(flow_difference_per_peer_UPLINK)

        values_of_Number_UPLINK_per_peer = list(Number_UPLINK_per_peer.values())
        if len(values_of_Number_UPLINK_per_peer) != 0:
            self.max_UPLINK_in_peer = max(values_of_Number_UPLINK_per_peer)
            self.min_UPLINK_in_peer = min(values_of_Number_UPLINK_per_peer)

        self.link_difference_in_UPLINK = self.max_UPLINK_in_peer - self.min_UPLINK_in_peer

    def get_DOWNLINK_value(self):

        Number_DOWNLINK_per_peer = dict()
        Number_of_DOWNLINK_with_a_null_flow_per_peer = dict()
        Number_of_DOWNLINK_with_low_flow_per_peer = dict()
        all_peers = dict()  # PEER {peer:[load1, load2, load3] , peer2:[load1, load2, load3] }
        all_label_per_peers = dict()  # PEER {peer:[#1, #2, #3] , peer2:[#1, #2, #3] }
        flow_difference_per_peer_DOWNLINK = []
        Directional = 1

        for i, link in enumerate(self.Neighbor_list):

            if link in self.dictionary:

                for j, link_neighbor in enumerate(self.dictionary[link]):

                    try:

                        if link_neighbor[2]==self.name:

                            self.Two_way_flow[i] = 1 # flux à double sens

                            if link in all_peers:

                                all_peers[link].append(link_neighbor[1])
                                all_label_per_peers[link].append(link_neighbor[0])


                                if link_neighbor[1] <= 2:

                                    if link_neighbor[1] == 0:
                                        Number_of_DOWNLINK_with_a_null_flow_per_peer[link] += 1
                                    else:
                                        Number_of_DOWNLINK_with_low_flow_per_peer[link] += 1

                                Number_DOWNLINK_per_peer[link] += 1

                            else:
                                all_peers[link] = list()
                                all_label_per_peers[link] = list()
                                all_peers[link].append(link_neighbor[1])
                                all_label_per_peers[link].append(link_neighbor[0])
                                Number_of_DOWNLINK_with_a_null_flow_per_peer[link] = 0
                                Number_of_DOWNLINK_with_low_flow_per_peer[link] = 0

                                if link_neighbor[1] <= 2:

                                    if link_neighbor[1] == 0:
                                        Number_of_DOWNLINK_with_a_null_flow_per_peer[link] = 1
                                    else:
                                        Number_of_DOWNLINK_with_low_flow_per_peer[link] = 1

                                Number_DOWNLINK_per_peer[link] = 1

                    except:
                        print("Erreur Sur le node = ", link)
            else:
                self.Two_way_flow[i] = -1 # débit à uni-directionnel et noeud absend dans la topologie

        for peer, list_flows in all_peers.items():

            self.Sum_of_the_flows_entering_in_node += sum(list_flows)
            self.Total_number_of_DOWNLINK += len(list_flows)
            self.Number_DOWNLINK_per_peer.append(Number_DOWNLINK_per_peer[peer])

            self.Total_number_of_DOWNLINK_with_a_null_flow += Number_of_DOWNLINK_with_a_null_flow_per_peer[peer]
            self.Number_of_DOWNLINK_with_a_null_flow_per_peer.append(Number_of_DOWNLINK_with_a_null_flow_per_peer[peer])
            self.Total_number_of_DOWNLINK_with_a_low_flow += Number_of_DOWNLINK_with_low_flow_per_peer[peer]
            self.Number_of_DOWNLINK_with_low_flow_per_peer.append(Number_of_DOWNLINK_with_low_flow_per_peer[peer])
            self.DOWNLINK_flow_per_peer.append(list_flows)
            self.DOWNLINK_label_per_peer.append(all_label_per_peers[peer])
            flow_difference_per_peer_DOWNLINK.append(abs(max(list_flows)-min(list_flows)))

        self.Conservation_of_Flows = abs(self.Sum_of_the_flows_entering_in_node - self.Sum_of_the_flows_exiting_in_node)
        if len(flow_difference_per_peer_DOWNLINK) != 0:
            self.max_flow_difference_in_DOWNLINK = max(flow_difference_per_peer_DOWNLINK)
            self.min_flow_difference_in_DOWNLINK = min(flow_difference_per_peer_DOWNLINK)

        values_of_Number_DOWNLINK_per_peer = list(Number_DOWNLINK_per_peer.values())
        if len(values_of_Number_DOWNLINK_per_peer) != 0:
            self.max_DOWNLINK_in_peer = max(values_of_Number_DOWNLINK_per_peer)
            self.min_DOWNLINK_in_peer = min(values_of_Number_DOWNLINK_per_peer)

        self.link_difference_in_DOWNLINK = self.max_DOWNLINK_in_peer - self.min_DOWNLINK_in_peer
