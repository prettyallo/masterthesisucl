import pandas as pd
import numpy as np
import copy
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from toolz import compose, pluck, groupby, valmap, first, unique, get, countby
import plotly.express as px
#from .utils import maxValue
import time
#import datetime
from datetime import datetime
import ast
import random
import statistics




########## TOUVER UNE SOLUTION POUR UTILISER LES FONCTION DE UTILIS ############
## A SUPPRIMER ####


def maxValue(Dictionary):

    RESULT = list()
    #RESULT = dict()
    VALUES = list(Dictionary.values())
    KEYS = list(Dictionary.keys())

    MAX_VAL = max(VALUES)

    for i, val in enumerate(VALUES):
        if val == MAX_VAL:
            RESULT.append(KEYS[i])
            #RESULT[KEYS[i]] = Dictionary[KEYS[i]]

    return RESULT


#################################################################################


listpluck = compose(list, pluck)
TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

class View:
    graph_edit = 0
    def __init__(self, df):
        self.Data_OVH_events = pd.read_csv("/Users/prettyallo/Data/Master_1_Cybersecurite/Annee_2020_2021/MasterThesis/MasterThesisUCL/data/Data_OVH_events.csv")
        node_extra_domaine = []
        node_intra_domaine = []

        self.df = df
        self.high_timestamp = max(list(self.df['Date']))
        self.low_timestamp = min(list(self.df['Date']))
        self.layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(
                l=30,
                r=30,
                b=20,
                t=40
            ),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation='h'),
            title='Satellite Overview',
            mapbox=dict(
                #accesstoken=mapbox_access_token,
                style="light",
                center=dict(
                    lon=-78.05,
                    lat=42.54
                ),
                zoom=7,
            )
        )

        timestamp_max_node = max(maxValue(dict(df['Date'].value_counts())))
        self.timestamp_max_node = timestamp_max_node

        node_list = list(dict(df["Node"].value_counts()).keys())
        self.node_list = node_list

        for name in node_list:
            if name.isupper():
                node_extra_domaine.append(name)
            else:
                node_intra_domaine.append(name)

        self.node_extra_domaine = node_extra_domaine
        self.node_intra_domaine = node_intra_domaine



    def value_column(self, column, Node_type):

        if (Node_type == 0 ) or (Node_type == 1):
            dff = self.df[self.df['Node_type'] == Node_type]
        else:
            dff = self.df

        list_stocks = (dff.sort_values(column))[column].unique()
        dict_list = []
        for i in list_stocks:
            dict_list.append({'label': i, 'value': i})

        return dict_list


    def make_pie_figure(self, dff, TEXT_PIE_GRAPH):

        node_type = ['Intra domaine', 'Extra domaine']
        data_ = [len(self.node_intra_domaine), len(self.node_extra_domaine)]
        explode = (0.1, 0.0)
        colors = ("#e79a3c", "#3c9ae7")

        layout_pie = copy.deepcopy(self.layout)

        data = [
            dict(
                type='pie',
                labels= node_type,
                values=data_,
                name='Production Breakdown',
                #text=TEXT_PIE_GRAPH['text'],
                hoverinfo="text+value+percent",
                textinfo="label+percent+name",
                hole=0.4,
                shadow=True,
                colors=colors,
                startangle=90,
                explode=explode,
            ),
        ]

        layout_pie['title'] = TEXT_PIE_GRAPH['title']
        fig = dict(data=data, layout=layout_pie)

        return fig


    def make_listNode_figure(self, hoverdata, timer_detail):

        if hoverdata is None:
            date = datetime.fromtimestamp(self.low_timestamp).strftime("%d-%m-%Y %H:%M:%S")
            total_node2 = 0
        else:
            date = hoverdata['points'][0]['x']
            total_node2 = hoverdata['points'][0]['y']

        total_node = total_node2
        list_nodes = ["VIDE_NODE"]
        status_nodes = ['VIDE']
        color = ['white']
        dif = 0


        if timer_detail == 1:
            _status_nodes = list()
            _color = list()
            timestamp = time.mktime(datetime.strptime(str(date), "%d-%m-%Y %H:%M:%S").timetuple())
            _list_nodes = list(self.df[self.df['Date'] == timestamp]['Node'])

            for node in self.node_list:
                if node in _list_nodes:
                    _status_nodes.append("YES")
                    _color.append("white")
                else:
                    _status_nodes.append("*****-NO-****")
                    _color.append("red")
                    dif +=1

            status_nodes = _status_nodes
            list_nodes = self.node_list
            color = _color


        figure = {
            'data': [
                dict(
                    header=dict(
                        values=["Node", "Status"],
                        fill_color='paleturquoise',
                        align='left',
                        font={'color': 'white'},
                        fill={'color': 'black'}
                    ),
                    cells=dict(
                        values=[list_nodes, status_nodes],
                        fill_color='lavender', align='left',fill={'color': color}
                    ),
                    type="table",

                )
            ],
            'layout': dict(
                title={"text": str(date)+" |"+str(total_node)+"|"+str(dif)},
                margin={'l': 30, 'b': 15, 't': 65, 'r': 30})
        }

        return figure


    def make_scatter_figure(self, hoverData, xaxis_column, yaxis_column, timer_detail):
        df = self.df
        layout_aggregate = copy.deepcopy(self.layout)
        node_name = hoverData['points'][0]['customdata']
        dff = df[df['Node'] == node_name]

        #print("Nom sur Graph Courbe =| ", node_name)

        if timer_detail == 1:
            def convert_timestamp (timestamp) :
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        else:
            def convert_timestamp (timestamp) :
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        dff_x = dff[['Date', xaxis_column]]
        dict_date_sort_date_x = dict()
        dict_date_sort_date2_x = dict()
        for index, row in dff_x.iterrows():
            dict_date_sort_date_x[convert_timestamp(row['Date'])] = float(row[xaxis_column])
            dict_date_sort_date2_x[row['Date']] = float(row[xaxis_column])

        dff_y = dff[['Date', yaxis_column]]
        dict_date_sort_date_y = dict()
        dict_date_sort_date2_y = dict()
        for index, row in dff_y.iterrows():
            dict_date_sort_date_y[convert_timestamp(row['Date'])] = float(row[yaxis_column])
            dict_date_sort_date2_y[row['Date']] = float(row[yaxis_column])

        data = [
            dict(
                type='scatter',
                mode='lines',
                name=xaxis_column,
                x=list(dict_date_sort_date_x.keys()),
                y=list(dict_date_sort_date_x.values()),
                line=dict(
                    shape="spline",
                    smoothing="2",
                    color='#F9ADA0'
                )
            ),
            dict(
                type='scatter',
                mode='lines',
                name=yaxis_column,
                x=list(dict_date_sort_date_y.keys()),
                y=list(dict_date_sort_date_y.values()),
                line=dict(
                    shape="spline",
                    smoothing="2",
                    color='#849E68'
                )
            )

        ]
        layout_aggregate['title'] = str(node_name)

        figure = dict(data=data, layout=layout_aggregate)
        return figure

    def make_select_router_per_peer(self, hoverData):

        result = []
        df = self.df
        node_name = hoverData['points'][0]['customdata']
        dff = df[df['Node'] == node_name]


        tout_les_voisin_avec_doublon = list(dict(dff.Neighbor_list.value_counts()).keys())
        list_voisin = []
        for item in tout_les_voisin_avec_doublon:
            list_voisin.extend(ast.literal_eval(item))

        list_voisin = set(list_voisin)
        peer_number = len(list_voisin)

        for i in list_voisin:
            item = {}
            item['label']= i
            item['value'] = i
            result.append(item)

        return result, peer_number


    def make_scatter_figure2(self, hoverData, xaxis_column, yaxis_column, timer_detail):
        data = []
        list_voisin = []
        check_same_value = []
        df = self.df
        layout_aggregate = copy.deepcopy(self.layout)
        node_name = hoverData['points'][0]['customdata']
        dff = df[df['Node'] == node_name]
        dict_date_sort_date_y = dict()
        dict_date_sort_date2_y = dict()

        if timer_detail == 1:
            def convert_timestamp (timestamp) :
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        else:
            def convert_timestamp (timestamp) :
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        tout_les_voisin_avec_doublon = list(dict(dff.Neighbor_list.value_counts()).keys())
        for item in tout_les_voisin_avec_doublon:
            list_voisin.extend(ast.literal_eval(item))
        list_voisin = list(set(list_voisin))
        #print(list_voisin)

        if (yaxis_column == "UPLINK_flow_per_peer") or (yaxis_column == "DOWNLINK_flow_per_peer"):
            if yaxis_column == "UPLINK_flow_per_peer" :
                ORIENTATION = "UPLINK"
            else:
                ORIENTATION = "DOWNLINK"


            if xaxis_column in list_voisin:

                for index, row in dff.iterrows():

                    list_peer = ast.literal_eval(row['Neighbor_list'])
                    position_peer = list_peer.index(xaxis_column)
                    check_same_value.append(ast.literal_eval(row["Number_"+ORIENTATION+"_per_peer"] )[position_peer])
                    content_cell = ast.literal_eval(row[yaxis_column])

                    if position_peer < len(content_cell):
                        dict_date_sort_date_y[convert_timestamp(row['Date'])] = content_cell[position_peer]
                        dict_date_sort_date2_y[row['Date']] = content_cell[position_peer]
                    else:
                        dict_date_sort_date_y[convert_timestamp(row['Date'])] = -1
                        dict_date_sort_date2_y[row['Date']] = -1

                #print("MAX | MIN NOMBRE DE  LIENS  = ", max(check_same_value), "|", min(check_same_value) )

                number_of_colors = max(check_same_value)
                color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
                         for i in range(number_of_colors)]


                for i, link in enumerate(range(0,max(check_same_value)) ):
                    axe_fig = dict()
                    for timestamp, link_list_flow in dict_date_sort_date_y.items():
                        axe_fig[timestamp] = link_list_flow[link]

                    fig_x = dict(
                        type='scatter',
                        mode='lines',
                        name="#"+str(link),
                        x=list(axe_fig.keys()),
                        y=list(axe_fig.values()),
                        line=dict(
                            shape="spline",
                            smoothing="2",
                            color=color[i]
                        )
                    )

                    data.append(fig_x)
            else:

                fig_x = dict(
                    type='scatter',
                    mode='lines',
                    name="#" ,
                    x=[-1],
                    y=[-1],
                    line=dict(
                        shape="spline",
                        smoothing="2",
                        color='#849E68'
                    )
                )

                data.append(fig_x)

        else:

            dict_date_sort_date_y = dict()
            dict_date_sort_date2_y = dict()
            for index, row in dff.iterrows():
                list_peer = ast.literal_eval(row['Neighbor_list'])

                # print('Neighbor_list', '= ', ast.literal_eval(row['Neighbor_list']))
                # print(yaxis_column, '= ',ast.literal_eval(row[yaxis_column] ))

                if xaxis_column in list_voisin:
                    position_peer = list_peer.index(xaxis_column)
                    content_cell = ast.literal_eval(row[yaxis_column] )
                    if position_peer < len(content_cell):
                        dict_date_sort_date_y[convert_timestamp(row['Date'])] = content_cell[position_peer]
                        dict_date_sort_date2_y[row['Date']] = content_cell[position_peer]
                    else:
                        dict_date_sort_date_y[convert_timestamp(row['Date'])] = -1
                        dict_date_sort_date2_y[row['Date']] = -1
                else:
                    dict_date_sort_date_y[convert_timestamp(row['Date'])] = -1
                    dict_date_sort_date2_y[row['Date']] = -1


            fig_x=dict(
                type='scatter',
                mode='lines',
                name=yaxis_column,
                x=list(dict_date_sort_date_y.keys()),
                y=list(dict_date_sort_date_y.values()),
                line=dict(
                    shape="spline",
                    smoothing="2",
                    color='#849E68'
                )
            )

            data.append(fig_x)

        layout_aggregate['title'] = str(yaxis_column)
        figure = dict(data=data, layout=layout_aggregate)

        return figure



    def make_bar_chart(self, dff, TEXT_APP, timer_detail):
        dict_date_sort_date = dict()
        dict_date_sort_date2 = dict()
        dict_date = dict(dff['Date'].value_counts())

        if timer_detail==1:
            def convert_timestamp (timestamp) :
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')

        else:
            def convert_timestamp (timestamp) :
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')


        for items in sorted(dict_date.keys()):
            dict_date_sort_date[convert_timestamp(items)] = dict_date[items]
            dict_date_sort_date2[items] = dict_date[items]

        figure = go.Figure(
            data=[
                go.Bar(
                    name="Original",
                    x=list(dict_date_sort_date.keys()),
                    y=list(dict_date_sort_date.values()),
                    offsetgroup=0,
                ),
            ],
            layout=go.Layout(
                title=TEXT_APP['title_graph_figure'],
                yaxis_title="Nombre de routeur"

            )
        )


        return figure

    def make_main_figure(self, dff, xaxis_column, yaxis_column, date_):

        value_fig = {}

        print("TAILLE DU DAT SET", dff.shape)

        select_timestamp = list(dff.loc[dff['Date'] >= date_['min'] ] ['Date'])
        dff = dff[dff['Date'] == sorted(select_timestamp)[0]]

        for index, row in dff.iterrows():
            value_fig[row['Node']] = list()
            value_fig[row['Node']].append(row[xaxis_column])
            value_fig[row['Node']].append(row[yaxis_column])
            value_fig[row['Node']].append(row['Node_type'])


        fig = px.scatter(
            x=[i[0] for i in list(value_fig.values())],
            y=[i[1] for i in list(value_fig.values())],
            color=[i[2] for i in list(value_fig.values() ) ],
            hover_name=list(value_fig.keys())
        )

        fig.update_traces(customdata=dff['Node'])

        fig.update_xaxes(title=xaxis_column, type='linear')
        fig.update_yaxes(title=yaxis_column, type='linear')
        fig.update_layout(margin={'l': 40, 'b': 40, 't': 10, 'r': 0}, hovermode='closest')

        return fig

    def make_flow_sankey_figure(self):

        dff = self.df[self.df['Date']==self.timestamp_max_node]
        label_list = list(dff['Node'].unique())

        label_index = dict()
        color_index = dict()
        for index, node_name in enumerate(label_list):
            label_index[node_name] = index
            color_index[node_name] = '#%02x%02x%02x'%(0, 128, index)

        color_node = list(dff['Node'].map(color_index))
        source = list(dff['Node'].map(label_index))
        target = list(dff['neighbor_with_max_UPLINK'].map(label_index))
        values = list(dff['max_UPLINK_load'])

        sankey_figure = go.Figure(go.Sankey(
            valueformat=".0f",
            valuesuffix="%",
            node=dict(
                pad=3,
                thickness=15,
                # line=dict(color="black", width=0.5),
                label=label_list,
                #color=color_node
            ),
            link=dict(
                source=source,
                target=target,
                value=values,
                # label=label_list
            )))

        return sankey_figure

    def make_cytoscape_figure_nodes(self):

        following_node_di = {}  # user id -> list of users they are following
        following_edges_di = {}  # user id -> list of cy edges starting from user id

        followers_node_di = {}  # user id -> list of followers (cy_node format)
        followers_edges_di = {}  # user id -> list of cy edges ending at user id

        #dff = self.df[self.df['Date']==1630467910]
        dff = self.df[self.df['Date'] == self.timestamp_max_node]
        elements = list()
        list_nodes = list(dff['Node'].unique())
        cy_nodes = []
        cy_edges = []

        for index, node in enumerate(list_nodes):
            options = dict()
            if node.isupper(): options['classes'] = '0'
            else: options['classes'] = '1'
            options['data'] = {'id': node, 'label': node}
            #options['position'] = {'x': index, 'y': index + 3}
            cy_nodes.append(options)

        for index, row in dff.iterrows():



            options = dict()
            source = row['Node']
            target = row['neighbor_with_max_UPLINK']
            weight = row['max_UPLINK_load']

            options['data'] = {'id': source+target, 'source': source, 'target': target,'weight': weight }
            cy_edges.append(options)

            # Process dictionary of following
            cy_target = {"data": {"id": target, "label": "User #" + str(target[-5:])}}
            cy_source = {"data": {"id": source, "label": "User #" + str(source[-5:])}}
            cy_edge = options
            if not following_node_di.get(source):
                following_node_di[source] = []
            if not following_edges_di.get(source):
                following_edges_di[source] = []

            following_node_di[source].append(cy_target)
            following_edges_di[source].append(cy_edge)

            # Process dictionary of followers
            if not followers_node_di.get(target):
                followers_node_di[target] = []
            if not followers_edges_di.get(target):
                followers_edges_di[target] = []

            followers_node_di[target].append(cy_source)
            followers_edges_di[target].append(cy_edge)

        elements =   cy_nodes + cy_edges

        return elements, following_node_di, following_edges_di, followers_node_di, followers_edges_di





    def grapheNumericalValue (self, hoverData, timer_detail) :

        data = []
        df = self.df
        layout_aggregate = copy.deepcopy(self.layout)
        #yaxis_column = [i for i in list(df.drop(['Date', 'Node_type', 'Conservation_of_Flows', 'Sum_of_the_flows_entering_in_node', 'Sum_of_the_flows_exiting_in_node'], axis=1).select_dtypes('number').columns)]
        yaxis_column = [i for i in list(df.drop(['Date', 'Node_type'], axis=1).select_dtypes('number').columns)]
        node_name = hoverData['points'][0]['customdata']
        dff = df[df['Node'] == node_name]


        number_of_colors = len(yaxis_column)
        color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(9)])
                 for i in range(number_of_colors)]

        if timer_detail == 1:
            def convert_timestamp(timestamp):
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        else:
            def convert_timestamp(timestamp):
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        for i,yaxis in enumerate(yaxis_column):

            dff_y = dff[['Date', yaxis]]
            dict_date_sort_date_y = dict()
            dict_date_sort_date2_y = dict()
            for index, row in dff_y.iterrows():
                dict_date_sort_date_y[convert_timestamp(row['Date'])] = float(row[yaxis])
                dict_date_sort_date2_y[row['Date']] = float(row[yaxis])

            fig_x = dict(
                type='scatter',
                mode='lines',
                name=yaxis,
                x=list(dict_date_sort_date_y.keys()),
                y=list(dict_date_sort_date_y.values()),
                line=dict(
                    shape="spline",
                    smoothing="2",
                    color=color[i]
                )
            )
            data.append(fig_x)

        layout_aggregate['title'] = str(node_name)
        figure = dict(data=data, layout=layout_aggregate)

        return figure


    def make_evolution_routeur(self, timer_detail):
        layout_aggregate = copy.deepcopy(self.layout)
        dict_date_sort_date = dict()
        dict_date = dict(self.df['Date'].value_counts())

        if timer_detail == 1:
            def convert_timestamp(timestamp):
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y %H:%M:%S')
        else:
            def convert_timestamp(timestamp):
                return datetime.fromtimestamp(timestamp).strftime('%d-%m-%Y')

        for items in sorted(dict_date.keys()):
            dict_date_sort_date[convert_timestamp(items)] = dict_date[items]

        data = [
            dict(
                type='scatter',
                mode='lines',
                name='Evoltion du nombre de routeur',
                x=list(dict_date_sort_date.keys()),
                y=list(dict_date_sort_date.values()),
                line=dict(
                    shape="spline",
                    smoothing="2",
                    color='#F9ADA0'
                )
            )
        ]
        layout_aggregate['title'] = "Evoltion du nombre de routeur"

        figure = dict(data=data, layout=layout_aggregate)
        return figure


class Settings:

    def __init__(self, df, start_date, end_date, hour_slider, Node_type, dead_links_select, dead_links_select_user_value, load_diff_links, directional_link, load_diff_links_select_user_value, diff_peer):
        self.df = df
        self.start_date = start_date
        self.end_date = end_date
        self.hour_slider = hour_slider
        self.Node_type = Node_type
        self.dead_links_select = dead_links_select
        self.diff_peer = diff_peer

        if len(load_diff_links_select_user_value)==0:
            self.load_diff_links_select_user_value = [0]
        else:
            self.load_diff_links_select_user_value = load_diff_links_select_user_value

        if len(dead_links_select_user_value) == 0:
            self.dead_links_select_user_value = [0]
        else:
            self.dead_links_select_user_value = dead_links_select_user_value

        self.load_diff_links = load_diff_links
        self.directional_link = directional_link

        self.node_list = sorted(dict(df['Node'].value_counts()).keys())

    def get_date(self):
        date = dict()
        sart_timestamp = str(self.start_date) + " " + str(self.hour_slider[0]) + ":00:00"
        end_timestamp = str(self.end_date) + " " + str(self.hour_slider[1]) + ":00:00"
        date['min'] = time.mktime(datetime.strptime(sart_timestamp, "%Y-%m-%d %H:%M:%S").timetuple())
        date['max'] = time.mktime(datetime.strptime(end_timestamp, "%Y-%m-%d %H:%M:%S").timetuple())
        return date

    def get_Node_type(self):
        if self.Node_type == 2:
            return [1, 0]
        else:
            return [self.Node_type]

    def get_dead_links_select(self):

        null_flow = []
        no_specific_null_flow = []

        if len(self.directional_link) == 1:

            null_flow.append(sorted(list(dict(self.df['Total_number_of_' +self.directional_link[0] +'_with_a_null_flow'].value_counts()).keys())) )
            null_flow[0] = list(set(sorted(null_flow[0])))[1:]

            no_specific_null_flow.append(null_flow[0])
            no_specific_null_flow[0].append(0)

        else:

            null_flow.append(sorted(list(dict(self.df['Total_number_of_' + self.directional_link[0] + '_with_a_null_flow'].value_counts()).keys())) )
            null_flow[0] = list(set(sorted(null_flow[0])))[1:]

            null_flow.append(sorted(list(dict(self.df['Total_number_of_' + self.directional_link[1] + '_with_a_null_flow'].value_counts()).keys())) )
            null_flow[1] = list(set(sorted(null_flow[1])))[1:]

            no_specific_null_flow.append([0])
            no_specific_null_flow[0].extend(null_flow[0])
            no_specific_null_flow.append([0])
            no_specific_null_flow[1].extend(null_flow[1])

        select_link = dict(
            no_specific_null_flow=no_specific_null_flow,
            null_flow=null_flow,
            no_null_flow =[[0], [0]],
            select_null_flow_number = [self.dead_links_select_user_value, self.dead_links_select_user_value]
        )

        return select_link[self.dead_links_select]

    def get_load_diff_links(self):

        diff_flow = []
        no_specific_diff_flow = []

        if len(self.directional_link) == 1:
            diff_flow.append(sorted(list(dict(self.df['min_flow_difference_in_' + self.directional_link[0]].value_counts()).keys())) )
            diff_flow[0].extend(sorted(list(dict(self.df['max_flow_difference_in_' + self.directional_link[0]].value_counts()).keys())) )
            diff_flow[0] = list(set(sorted(diff_flow[0])))[1:]

            no_specific_diff_flow.append(diff_flow[0])
            no_specific_diff_flow[0].append(0)

        else:

            diff_flow.append(sorted(list(dict(self.df['min_flow_difference_in_' + self.directional_link[0]].value_counts()).keys())) )
            diff_flow[0].extend(sorted(list(dict(self.df['max_flow_difference_in_' + self.directional_link[0]].value_counts()).keys())) )
            diff_flow[0] = list(set(sorted(diff_flow[0])))[1:]

            diff_flow.append(sorted(list(dict(self.df['min_flow_difference_in_' + self.directional_link[1]].value_counts()).keys())) )
            diff_flow[1].extend(sorted(list(dict(self.df['max_flow_difference_in_' + self.directional_link[1]].value_counts()).keys())) )
            diff_flow[1] = list(set(sorted(diff_flow[1])))[1:]

            no_specific_diff_flow.append([0])
            no_specific_diff_flow[0].extend(diff_flow[0])
            no_specific_diff_flow.append([0])
            no_specific_diff_flow[1].extend(diff_flow[1])

        select_link = dict(
            no_specific_diff_flow=no_specific_diff_flow,
            diff_flow = diff_flow,
            no_diff_flow=[[0],[0]],
            select_null_flow_number=[self.load_diff_links_select_user_value, self.load_diff_links_select_user_value]
        )

        return select_link[self.load_diff_links]

    def get_diff_number_of_peer(self):
        yes_node_in_dff_peer = []
        no_node_in_dff_peer = []
        node_list = self.node_list
        for node in node_list:
            liste_num_peer = sorted(dict(self.df[self.df['Node']==node]['Number_of_neighbours'].value_counts()).keys())
            if len(liste_num_peer) <= 1:
                no_node_in_dff_peer.append(node)
            else:
                yes_node_in_dff_peer.append(node)

        select_link = dict(
            YES=yes_node_in_dff_peer,
            NO=no_node_in_dff_peer,
            TOUS=node_list

        )

        return select_link[self.diff_peer]

    def da_all_incidence2(self):
        Newdf = pd.DataFrame()
        node_list = self.node_list
        node_have_null_flow = {}
        node_have_not_null_flow = {}

        node_have_low_flow = {}
        node_have_not_low_flow = {}

        for node in node_list:
            dict_number_of_links_a_null_flow = dict(self.df[self.df['Node'] == node]['Total_number_of_UPLINK_with_a_null_flow'].value_counts())
            dict_number_of_links_a_low_flow = dict(self.df[self.df['Node'] == node]['Total_number_of_UPLINK_with_a_low_flow'].value_counts())

            if len(sorted(dict_number_of_links_a_null_flow.keys())) != 1: # ce nod a des liens mort
                node_have_null_flow[node] = sorted(dict_number_of_links_a_null_flow.keys())
            else:
                node_have_not_null_flow[node] = sorted(dict_number_of_links_a_null_flow.keys())


            if len(sorted(dict_number_of_links_a_low_flow.keys())) != 1: # ce nod a des liens mort
                node_have_low_flow[node] = sorted(dict_number_of_links_a_low_flow.keys())
            else:
                node_have_not_low_flow[node] = sorted(dict_number_of_links_a_low_flow.keys())

        for node in node_list:
            dff = self.df[
                self.df['Node'].isin([node])
                & self.df['Total_number_of_UPLINK_with_a_null_flow'].isin(node_have_null_flow[node])
                & self.df['Total_number_of_UPLINK_with_a_low_flow'].isin(node_have_low_flow[node])
                ]
            Newdf = pd.concat([Newdf, dff], ignore_index=True)


        return Newdf









