################################################################################
# Import required libraries
#################################################################################
import json
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Output, Input, State
from toolz import compose, pluck, groupby, valmap, first, unique, get, countby
from datetime import date
from datetime import datetime

import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
import pickle
import copy
import math

from src.utils import *
from src.graphs import *
from controls import *
import tkinter
from tkinter import filedialog
import dash_cytoscape as cyto




################################################################################
# HELPERS
################################################################################
listpluck = compose(list, pluck)
listfilter = compose(list, filter)
listmap = compose(list, map)
listunique = compose(list, unique)

TIMESTAMP_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
lang = "fr_fr"

layout = dict(
            autosize=True,
            automargin=True,
            margin=dict(
                l=30,
                r=30,
                b=20,
                t=40
            ),
            hovermode="closest",
            plot_bgcolor="#F9F9F9",
            paper_bgcolor="#F9F9F9",
            legend=dict(font=dict(size=10), orientation='h'),
            title='Satellite Overview',
            mapbox=dict(
                #accesstoken=mapbox_access_token,
                style="light",
                center=dict(
                    lon=-78.05,
                    lat=42.54
                ),
                zoom=7,
            )
        )


stylesheet_config = [
    {
        "selector": 'node',
        'style': {
            "opacity": 0.65,
            'z-index': 9999
        }
    },
    {
        "selector": 'edge',
        'style': {
            "curve-style": "bezier",
            "opacity": 0.45,
            'z-index': 5000
        }
    },
    {
        'selector': '.followerNode',
        'style': {
            'background-color': '#04CF2F'
        }
    },
    {
        'selector': '.followerEdge',
        "style": {
            "mid-target-arrow-color": "blue",
            "mid-target-arrow-shape": "vee",
            "line-color": "#024E09"
        }
    },
    {
        'selector': '.followingNode',
        'style': {
            'background-color': '#F23127'
        }
    },
    {
        'selector': '.followingEdge',
        "style": {
            "mid-target-arrow-color": "red",
            "mid-target-arrow-shape": "vee",
            "line-color": "#910B04",
        }
    },
    {
        "selector": '.genesis',
        "style": {
            'background-color': '#B10DC9',
            "border-width": 2,
            "border-color": "purple",
            "border-opacity": 1,
            "opacity": 1,

            "label": "data(label)",
            "color": "#B10DC9",
            "text-opacity": 1,
            "font-size": 12,
            'z-index': 9999
        }
    },
    {
        'selector': ':selected',
        "style": {
            "border-width": 2,
            "border-color": "black",
            "border-opacity": 1,
            "opacity": 1,
            "label": "data(label)",
            "color": "black",
            "font-size": 12,
            'z-index': 9999
        }
    },
    {
        'selector':'.0',
        'style': {
            'background-color': '#ff7f00',
            'label': 'data(label)'
        },
    },

    {
        'selector':'.1',
        'style': {
            'background-color': '#3791D7',
            'label': 'data(label)'
        },
    },
]
################################################################################
# Initialise the app
################################################################################

# Load app
app = dash.Dash(__name__, title=TEXT_APP[lang]['title_tab'])
server = app.server


def load_csv_file(list_of_names, path='data/'):
    df = pd.DataFrame()

    for file in list_of_names:
        input = path+str(file)
        dff = pd.read_csv(input).sort_values(by=['Date'])
        df = pd.concat([df, dff], ignore_index=True)

    return df.sort_values(by=['Date'])

# Load data
list_of_name = ['df_08-2021.csv','df_09-2021.csv','df_10-2021.csv']

df = load_csv_file(list_of_name)
df_original = df
view = View(df)

high_timestamp = timestamp_printf(view.high_timestamp)
low_timestamp = timestamp_printf(view.low_timestamp)


################################################################################
# HELPERS
################################################################################
data_summary_filtered_md_template = TEXT_APP[lang]['row_selected']
data_summary_filtered_md = data_summary_filtered_md_template.format(len(df_original))


################################################################################
# LAYOUT
################################################################################

# Create app layout

app.layout = html.Div(
    [
        dcc.Store(id='aggregate_data'),
        html.Div(  ## 1) logo, Titre et bare de rechercher
            [
                html.Div(
                    [
                        html.H2(
                            'Analysing a data on OVH: cloud provider',

                        ),
                        html.H4(
                            'Overview of loads on links',
                        )
                    ],

                    className='eight columns'
                ),
                html.Img(
                    className='two columns',
                ),
                html.Div([
                    dcc.Upload(
                        id='upload_file',
                        children=html.Div([
                            'Drag and Drop or ',
                            html.A('Select Files')
                        ]),
                        style={
                            'width': '100%',
                            'height': '60px',
                            'lineHeight': '60px',
                            'borderWidth': '1px',
                            'borderStyle': 'dashed',
                            'borderRadius': '5px',
                            'textAlign': 'center',
                            'margin': '10px'
                        },
                        # Allow multiple files to be uploaded
                        multiple=True,
                        # accept = '.csv'
                    ),
                ]
                )
            ],
            id="header",
            className='row',
        ),

        dcc.Tabs(
            [
################################################################# PARTIE 1 : Data visualisation #############################################
                dcc.Tab(label='Data visualisation',
                        children=[
                            html.Div(
                                [
                                    html.Div(
                                        [
                                            html.P(
                                                TEXT_APP[lang]['date_range'],
                                                className="control_label"
                                            ),
                                            dcc.DatePickerRange(
                                                id = 'date_range',
                                                display_format='D/M/Y',
                                                start_date=date(low_timestamp.year, low_timestamp.month, low_timestamp.day),
                                                end_date=date(high_timestamp.year, high_timestamp.month, high_timestamp.day),
                                                start_date_placeholder_text="Start Period",
                                                end_date_placeholder_text="End Period",
                                                calendar_orientation='horizontal',
                                            ),
                                            html.P(
                                                TEXT_APP[lang]['hour_slider'],
                                                className="control_label"
                                            ),
                                            dcc.RangeSlider(
                                                id='hour_slider',
                                                min=0,
                                                max=23,
                                                value=[0, 23],
                                                marks={i: str(i) for i in range(0, 23)},
                                                className="dcc_control"
                                            ),
                                            html.Label('Link management:', style={'paddingTop': '2rem', 'display': 'inline-block'}),
                                            dcc.Checklist(
                                                id='Directional_link',
                                                options=[
                                                    {'label': 'UPLINK', 'value': 'UPLINK'},
                                                    {'label': 'DOWNLINK', 'value': 'DOWNLINK'}
                                                ],
                                                value=['UPLINK', 'DOWNLINK'],
                                            ),
                                            html.P(
                                                TEXT_APP[lang]['router_type'],
                                                className="control_label"
                                            ),
                                            dcc.RadioItems(
                                                id='router_type',
                                                options=TEXT_APP[lang]['router_type_select'],
                                                value=2,
                                                labelStyle={'display': 'inline-block'},
                                                className="dcc_control"
                                            ),
                                            html.Br(),
                                            html.P(
                                                TEXT_APP[lang]['select_router'],
                                                className="control_label"
                                            ),
                                            dcc.Dropdown(
                                                id='select_router',
                                                # options=view.value_column('node'),
                                                multi=True,
                                                value=[],
                                                className="dcc_control"
                                            ),
                                            html.P(
                                                TEXT_APP[lang]['dead_links'],
                                                className="control_label"
                                            ),
                                            dcc.RadioItems(
                                                id='dead_links_select',
                                                options=TEXT_APP[lang]['dead_links_select'],
                                                value='no_specific_null_flow',
                                                labelStyle={'display': 'inline-block'},
                                                className="dcc_control",
                                            ),
                                            dcc.Dropdown(
                                                id="dead_links_select_user_value",
                                                options=[{'label': i, 'value': i} for i in sorted(list(dict(df_original.Total_number_of_UPLINK_with_a_null_flow.value_counts()).keys()))],
                                                multi=True,
                                                value=[],

                                            ),
                                            html.Br(),
                                            html.P(
                                                TEXT_APP[lang]['load_diff_links'],
                                                className="control_label"
                                            ),
                                            dcc.RadioItems(
                                                id='load_diff_links',
                                                options=TEXT_APP[lang]['load_diff_links_select'],
                                                value='no_specific_diff_flow',
                                                labelStyle={'display': 'inline-block'},
                                                className="dcc_control",
                                            ),
                                            dcc.Dropdown(
                                                id="load_diff_links_select_user_value",
                                                options=[{'label': i, 'value': i} for i in sorted(list(dict(df_original.max_flow_difference_in_UPLINK.value_counts()).keys()))],
                                                multi=True,
                                                value=[],

                                            ),
                                            html.Br(),
                                            html.Br(),
                                            html.P(
                                                "Change in number of neighbours",
                                                className="control_label"
                                            ),
                                            dcc.RadioItems(
                                                id='diff_peer',
                                                options=TEXT_APP[lang]['diff_peer'],
                                                value='TOUS',
                                                labelStyle={'display': 'inline-block'},
                                                className="dcc_control",
                                            ),
                                            html.Br(),
                                            html.P(
                                                "Number of links on node",
                                                className="control_label"
                                            ),
                                            html.Div([
                                                html.Label('Min link:', style={'paddingTop': '2rem', 'display': 'inline-block'}),
                                                dcc.Dropdown(
                                                    id="min_link",
                                                    options=[{'label': i, 'value': i} for i in sorted(list(dict(df_original.max_DOWNLINK_in_peer.value_counts()).keys())) ],
                                                    multi=True,
                                                    value=[],
                                                ),
                                            ], style={'width': '48%', 'display': 'inline-block'}),
                                            html.Div([
                                                html.Label('Max link:', style={'paddingTop': '2rem', 'display': 'inline-block'}),
                                                dcc.Dropdown(
                                                    id="max_link",
                                                    options=[{'label': i, 'value': i} for i in sorted(list(dict(df_original.max_DOWNLINK_in_peer.value_counts()).keys())) ],
                                                    multi=True,
                                                    value=[],
                                                ),
                                            ], style={'width': '48%', 'display': 'inline-block'}),
                                        ],
                                        className="pretty_container four columns"
                                    ),
                                    html.Div(
                                        [
                                            html.Div(
                                                [
                                                    html.Div(
                                                        [
                                                            dcc.Graph(id='pie_graph')

                                                        ],
                                                        className="six columns pretty_container"),
                                                    html.Div(
                                                        [
                                                            html.H3(id='number_of_router', style={'fontWeight': 'bold', 'color': '#f73600'}),
                                                            html.Label('Node', style={'paddingTop': '.3rem'}),
                                                        ], className="three columns number-stat-box",

                                                    ),
                                                    html.Div(
                                                        [
                                                            dcc.Loading(
                                                                [
                                                                    html.Div(id='loader-trigger-1', style={"display": "none"}),
                                                                    html.Div(id='loader-trigger-2', style={"display": "none"}),
                                                                    html.Div(id='loader-trigger-3', style={"display": "none"}),
                                                                    html.Div(id='loader-trigger-4', style={"display": "none"}),
                                                                    html.H3(id='loading_bar', children=data_summary_filtered_md, style={'fontWeight': 'bold', 'color': '#00aeef'}),
                                                                    html.Progress(id="selected_progress",
                                                                                  max=f"{len(df_original)}",
                                                                                  value=f"{len(df_original)}"

                                                                                  ),
                                                                    html.Label('instances', style={'paddingTop': '.3rem'}),
                                                                ],
                                                            ),
                                                        ],className="three columns number-stat-box"
                                                    ),
                                                    html.Div(children=[
                                                        html.P(id='files_list', style={'fontWeight': 'bold'}),
                                                        html.Label('Liste Dataset', style={'paddingTop': '.3rem'}),
                                                    ], className="three columns number-stat-box"),
                                                ],
                                                id="tripleContainer",
                                            ),
                                            html.Div(
                                                [
                                                    html.Div(
                                                        [
                                                        dcc.Graph(id='liste_of_routers',
                                                                  figure = {
                                                                    'data': [
                                                                        dict(
                                                                            header=dict(
                                                                                values=["Node", "Status"],
                                                                                fill_color='paleturquoise',
                                                                                align='left',
                                                                                font={'color': 'white'},
                                                                                fill={'color': 'black'}
                                                                            ),
                                                                            type="table",
                                                                        )
                                                                    ],
                                                                },
                                                            )
                                                        ],
                                                        className="four columns pretty_container",
                                                    ),

                                                    html.Div(
                                                        [
                                                            dcc.RadioItems(
                                                                id='bar_chart_timer_detail',
                                                                options=TEXT_APP[lang]['bar_chart_timer_detail'],
                                                                value=0,
                                                                labelStyle={'display': 'inline-block'},
                                                                className="dcc_control",
                                                            ),
                                                            dcc.Graph(
                                                                id='count_graph',
                                                            )
                                                        ],
                                                        id="countGraphContainer",
                                                        className="pretty_container"
                                                    ),

                                                ],
                                                id="infoContainer",
                                                className="row"
                                            ),

                                        ],
                                        id="rightCol",
                                        className="eight columns"
                                    )
                                ],
                                className="row"
                            ),
                            html.Div(
                                [
                                    html.Div(
                                        [
                                            dcc.Graph(
                                                id='main_graph',
                                                hoverData={'points': [{'customdata': 'fra1-lim1-g1-8k'}]}
                                            )
                                        ],
                                        className='pretty_container four columns',
                                    ),
                                    html.Div(
                                        [
                                            dcc.Graph(id='individual_graph')
                                        ],
                                        className='pretty_container eight columns',
                                    ),
                                ],
                                className='row'
                            ),
                            html.Div(
                                [
                                    html.Div( ## 3.1 : carte graphe
                                        [
                                            html.Div(
                                                        [
                                                            html.H3(id='peer_number', style={'fontWeight': 'bold', 'color': '#f73600'}),
                                                            html.Label('Node', style={'paddingTop': '.3rem'}),
                                                        ],

                                                    ),
                                            html.Div([
                                                html.Label('Neighbour list:', style={'paddingTop': '2rem', 'display': 'inline-block'}),
                                                dcc.Dropdown(
                                                    id="xaxis_column_per_peer",
                                                    multi=False,
                                                    value="fra-fr5-sbb1-nc5",

                                                ),
                                            ], style={'width': '48%', 'display': 'inline-block'}),
                                            html.Div([
                                                html.Label('Stat Neighbour:', style={'paddingTop': '2rem', 'display': 'inline-block'}),
                                                dcc.Dropdown(
                                                    id="yaxis_column_per_peer",
                                                    options=[{'label': i, 'value': i} for i in OPTION_APP[lang]['list_column_stat_per_peer'] ],
                                                    multi=False,
                                                    value="Number_UPLINK_per_peer",

                                                ),
                                            ], style={'width': '48%', 'display': 'inline-block'}),

                                        ],
                                        className='pretty_container four columns',
                                    ),
                                    html.Div(
                                        [
                                            dcc.Graph(id='statictique_per_peer')
                                        ],
                                        className='pretty_container eight columns',
                                    ),
                                ],
                                className='row'
                            ),
                        ]
                ),
################################################################# PARTIE 2 : Data analysis ##################################################
                dcc.Tab(label='Data analysis',
                        children=[
                            html.Div( ## 3.2 : courbe graphe
                                [
                                    dcc.Graph(id='da_statictique_per_peer')
                                ],
                            ),
                            html.Div(
                                [
                                    dcc.Graph(id='liste_of_date',
                                    )
                                ],
                            ),

                        ]

                ),

# ################################################################# PARTIE 3 :  result ############################################
#                 dcc.Tab(label='Abnormalities result',
#                         children=[
#
#                         ]
#                 ),
            ]
        )
    ],

    id="mainContainer",
    style={
        "display": "flex",
        "flex-direction": "column"
    }
)


################################################################################
# INTERACTION CALLBACKS
################################################################################


def filter_setting_df(df, date, router_type, dead_links_select, load_diff, directional_link, min_link, max_link, diff_peer):

    total_len_link = [i for i in sorted(list(dict(df_original.max_DOWNLINK_in_peer.value_counts()).keys()))]

    if len(min_link)==0 :
        min_link = total_len_link
    if len(max_link)==0 :
        max_link = total_len_link

    if len(directional_link) == 1:
        dff = df[
            df['Node_type'].isin(router_type)
            & df['Node'].isin(diff_peer)
            & df["Total_number_of_" + directional_link[0] + "_with_a_null_flow"].isin(dead_links_select[0])
            & (df['Date'] > date['min'])
            & (df['Date'] < date['max'])
            & df["min_flow_difference_in_" + directional_link[0]].isin(load_diff[0])
            & df["min_" + directional_link[0] + "_in_peer"].isin(min_link)
            & df["max_" + directional_link[0] + "_in_peer"].isin(max_link)
            ]
    else:
        dff = df[
            df['Node_type'].isin(router_type)
            & df['Node'].isin(diff_peer)
            & df["Total_number_of_" + directional_link[0] + "_with_a_null_flow"].isin(dead_links_select[0])
            & df["Total_number_of_" + directional_link[1] + "_with_a_null_flow"].isin(dead_links_select[1])
            & (df['Date'] > date['min'])
            & (df['Date'] < date['max'])
            & df["min_flow_difference_in_" + directional_link[0]].isin(load_diff[0])
            & df["max_flow_difference_in_" + directional_link[0]].isin(load_diff[0])
            & df["min_flow_difference_in_" + directional_link[1]].isin(load_diff[1])
            & df["max_flow_difference_in_" + directional_link[1]].isin(load_diff[1])
            & df["min_" + directional_link[0] + "_in_peer"].isin(min_link)
            & df["max_" + directional_link[0] + "_in_peer"].isin(max_link)
            & df["min_" + directional_link[1] + "_in_peer"].isin(min_link)
            & df["max_" + directional_link[1] + "_in_peer"].isin(max_link)
            ]



    return dff


def createa_timestamp(start_date, end_date, hour_slider):
    date = dict()
    sart_timestamp = str(start_date) + " " + str(hour_slider[0]) + ":00:00"
    end_timestamp = str(end_date) + " " + str(hour_slider[1]) + ":00:00"
    date['min'] = time.mktime(datetime.datetime.strptime(sart_timestamp, "%Y-%m-%d %H:%M:%S").timetuple())
    date['max'] = time.mktime(datetime.datetime.strptime(end_timestamp, "%Y-%m-%d %H:%M:%S").timetuple())

    return date



@app.callback(Output('files_list', 'children'),
              [Input('upload_file', 'contents')],
              State('upload_file', 'filename'),
              State('upload_file', 'last_modified'))
def upload_csv_file(list_of_contents, list_of_names, list_of_dates):

    if list_of_contents is not None:

        files_list = [
            html.Table(
                [html.Tr([html.Th(list_of_names[item]),
                          html.Td(datetime.fromtimestamp(list_of_dates[item]).strftime('%d/%m/%Y %H:%M:%S'))]) for
                 item, name in enumerate(list_of_names)]
            )
        ]

        return files_list

    else:

        files_list = [
            html.Table(
                [html.Tr([html.Th(list_of_name[item])]) for item, name in enumerate(list_of_name)]
            )
        ]

        return files_list




@app.callback([Output('select_router', 'options'),
               Output('number_of_router', 'children'),
               Output('select_router', 'value')],
              Input('router_type', 'value'))
def make_select_router(router_type):
    options = view.value_column('Node', router_type)
    number_of_router = len(options)
    return options, number_of_router, []


@app.callback([Output('loading_bar', 'children'),
               Output('selected_progress', 'value'),
               Output('pie_graph', 'figure'),
               Output('count_graph', 'figure')],
              [Input('date_range', 'start_date'),
               Input('date_range', 'end_date'),
               Input('hour_slider', 'value'),
               Input('router_type', 'value'),
               Input('dead_links_select', 'value'),
               Input('dead_links_select_user_value', 'value'),
               Input('load_diff_links', 'value'),
               Input('upload_file', 'contents'),
               Input('bar_chart_timer_detail', 'value'),
               Input('Directional_link', 'value'),
               Input('load_diff_links_select_user_value', 'value'),
               Input('min_link', 'value'),
               Input('max_link', 'value'),
               Input('diff_peer', 'value'),])
def callback_loading_bar(start_date, end_date, hour_slider, router_type, dead_links_select,dead_links_select_user_value, load_diff_links,
                         list_of_contents, timer_detail, directional_link, load_diff_links_select_user_value, min_link, max_link, diff_peer):



    settings = Settings(df, start_date, end_date, hour_slider, router_type, dead_links_select,
                        dead_links_select_user_value, load_diff_links, directional_link, load_diff_links_select_user_value, diff_peer)

    dff = filter_setting_df(
        df,
        settings.get_date(),
        settings.get_Node_type(),
        settings.get_dead_links_select(),
        settings.get_load_diff_links(),
        directional_link,
        min_link,
        max_link,
        settings.get_diff_number_of_peer()
    )

    count = dff.shape[0]
    markdown_text = data_summary_filtered_md_template.format(count)


    loading_bar = str(count)
    df_bar = CreateDataFrameFast(dff, start_date, end_date, hour_slider)


    return markdown_text, loading_bar, view.make_pie_figure(dff, TEXT_PIE_GRAPH[lang]), view.make_bar_chart(
        df_bar, TEXT_APP[lang], timer_detail)


@app.callback(Output('main_graph', 'figure'),
              [Input('date_range', 'start_date'),
               Input('date_range', 'end_date'),
               Input('hour_slider', 'value'),
               Input('router_type', 'value'),
               Input('dead_links_select', 'value'),
               Input('dead_links_select_user_value', 'value'),
               Input('load_diff_links', 'value'),
               Input('select_router', 'value'),
               Input('Directional_link', 'value'),
               Input('load_diff_links_select_user_value', 'value'),
               Input('min_link', 'value'),
               Input('max_link', 'value'),
               Input('diff_peer', 'value')],
              )
def make_main_figure(start_date, end_date, hour_slider, router_type, dead_links_select,dead_links_select_user_value,
                     load_diff_links, select_router, directional_link, load_diff_links_select_user_value, min_link, max_link, diff_peer):

    # df = load_csv_file(list_of_name)
    # df_original = df
    # view = View(df)

    settings = Settings(df, start_date, end_date, hour_slider, router_type, dead_links_select,
                        dead_links_select_user_value, load_diff_links, directional_link, load_diff_links_select_user_value, diff_peer)

    dff = filter_setting_df(
        df,
        settings.get_date(),
        settings.get_Node_type(),
        settings.get_dead_links_select(),
        settings.get_load_diff_links(),
        directional_link,
        min_link,
        max_link,
        settings.get_diff_number_of_peer()
    )


    if len(select_router)!=0:
        dff = CreateDataFrameFast(dff, start_date, end_date, hour_slider)
        dff = dff[dff['Node'].isin(select_router)]

    # make_main_figure
    date_ = settings.get_date()
    main_figure = view.make_main_figure(dff,'Sum_of_the_flows_entering_in_node', 'Sum_of_the_flows_exiting_in_node', date_)


    return main_figure



@app.callback(Output('liste_of_routers', 'figure'),
              [Input('count_graph', 'hoverData'),
              Input('bar_chart_timer_detail', 'value')])
def make_list_node_by_date(hoverdata, timer_detail):
    figure = view.make_listNode_figure(hoverdata, timer_detail)

    return figure



# Main graph -> individual graph
@app.callback(Output('individual_graph', 'figure'),
              [Input('main_graph', 'hoverData'),
               Input('bar_chart_timer_detail', 'value')])
def make_individual_figure(hoverData, timer_detail):
    # df = load_csv_file(list_of_name)
    # df_original = df
    # view = View(df)

    figure = view.grapheNumericalValue(hoverData, timer_detail)

    return figure



@app.callback([Output('xaxis_column_per_peer', 'options'),
               Output('xaxis_column_per_peer', 'value'),
               Output('peer_number', 'children')],
              Input('main_graph', 'hoverData'))
def make_select_router_per_peer(hoverData):
    options, peer_number = view.make_select_router_per_peer(hoverData)
    return options, "", peer_number


# Main graph -> statictique_per_peer graph
@app.callback(Output('statictique_per_peer', 'figure'),
              [Input('main_graph', 'hoverData'),
               Input('xaxis_column_per_peer', 'value'),
               Input('yaxis_column_per_peer', 'value'),
               Input('bar_chart_timer_detail', 'value')])
def make_statictique_per_peer_figure(hoverData, xaxis_column, yaxis_column, timer_detail):
    # df = load_csv_file(list_of_name)
    # df_original = df
    # view = View(df)

    figure = view.make_scatter_figure2(hoverData, xaxis_column, yaxis_column, timer_detail)

    return figure


if __name__ == '__main__':
    # Run the app
    app.run_server(debug=True)
