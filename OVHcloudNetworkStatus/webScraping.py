# importation librairies
import os
from datetime import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pickle

import time
import datetime
import collections


import requests
import urllib.request
from bs4 import BeautifulSoup
import bs4
import re

from datetime import datetime
import calendar
from collections import OrderedDict



def web_scraping(pages) :
    i = 0
    code = []
    name = []
    message = []
    impact = []
    timesPubli = []
    updateDate = []
    start_time = []
    end_time = []


    timestamp = []

    print("START web scraping")

    for page in pages:

        url = "https://network.status-ovhcloud.com/history?page=" + str(page)

        req = requests.get(url)
        soup = BeautifulSoup(req.content, 'html.parser')

        historyIndex = soup.find_all("div", attrs={"data-react-class": "HistoryIndex"})[0]
        className = "data-react-props=\""
        endTag = "\"></div>"
        stringHistoryIndex = str(historyIndex)

        startIndex = stringHistoryIndex.index(className)
        endIndex = stringHistoryIndex.index(endTag)
        bruteText = stringHistoryIndex[startIndex + len(className):endIndex]

        soup2 = BeautifulSoup(bruteText, "html.parser")
        content = str(soup2)
        content = content.replace("true", "True")
        content = content.replace("false", "False")
        content = content.replace("null", "None")
        dataDictionary = eval(content)

        for months in dataDictionary['months']:
            for incidents in months['incidents']:

                liste_time = ""
                s_time = ""
                e_time = ""
                dd = incidents['timestamp']
                year = months['year']
                if incidents['message'].find("<i>") != -1:

                    start_indices = [j.start() for j in re.finditer("<i>", incidents['message'])]
                    end_indices = [k.start() for k in re.finditer("</i>", incidents['message'])]

                    for ei, si in enumerate(start_indices):
                        liste_time = liste_time + incidents['message'][si + 3:end_indices[ei]] + "\n"

                    if (incidents['message'].find("Start time") != -1) and (
                            incidents['message'].find("End time") != -1):
                        str_starts = incidents['message'][incidents['message'].index("Start time"):]
                        str_ends = incidents['message'][incidents['message'].index("End time"):]

                        str_start = str_starts[:str_starts.index("\n")]
                        try:
                            str_end = str_ends[:str_ends.index("\n")]
                        except:
                            str_end = str_ends

                        s_time = str_start[str_start.index("/") - 2:]
                        e_time = str_end[str_end.index("/") - 2:]

                        if s_time.find("</b>") != -1:
                            s_time = s_time[5:]
                        if e_time.find("</b>") != -1:
                            e_time = e_time[5:]

                else:
                    if (incidents['message'].find("Start time") != -1) and (
                            incidents['message'].find("End time") != -1):
                        str_starts = incidents['message'][incidents['message'].index("Start time"):]
                        str_ends = incidents['message'][incidents['message'].index("End time"):]

                        str_start = str_starts[:str_starts.index("\n")]
                        try:
                            str_end = str_ends[:str_ends.index("\n")]
                        except:
                            str_end = str_ends

                        s_time = str_start[str_start.index("/") - 2:]
                        e_time = str_end[str_end.index("/") - 2:]

                        if s_time.find("</b>") != -1:
                            s_time = s_time[5:]
                        if e_time.find("</b>") != -1:
                            e_time = e_time[5:]

                month_name = dd[0:3]
                year_number = str(year)
                day_number = dd[dd.index(">") + 1:dd.index("</var>")]
                Hours_number = dd[dd.index("time'>") + 6:][:dd[dd.index("time'>") + 6:].index("<")]
                month_number = str(datetime.strptime(month_name, "%b").month)

                timestamp.append(incidents['timestamp'])

                name.append(incidents['name'])
                impact.append(incidents['impact'])
                timesPubli.append(day_number + "/" + month_number + "/" + year_number + " " + Hours_number + " UTC")
                message.append(incidents['message'])
                code.append("https://network.status-ovhcloud.com/incidents/" + incidents['code'])

                updateDate.append(liste_time)
                start_time.append(s_time)
                end_time.append(e_time)

                print("SAVE EVENT = ", incidents['name'])

                i += 1



    # pandas dataframe
    Data_OVH_events = pd.DataFrame({
        'name': name,
        'impact': impact,
        'timesPubli-planing': timesPubli,
        'updateDate': updateDate,
        'start_time': start_time,
        'end_time': end_time,
        'message': message,
        'code': code})

    print("END web scraping. TOTAL EVENTS = ",i)
    return Data_OVH_events









def extractPrefix(cellule):
    if cellule.find("/") != -1:
        sep = "/"
    elif cellule.find("-") != -1:
        sep = "-"

    listDC = list()
    NbDC = [j.start() for j in re.finditer(sep, cellule)]
    prefix = cellule[:NbDC[0] - 1]
    listDC.append(cellule[:NbDC[0]])

    if len(NbDC) >= 2:
        if (abs(NbDC[0] - NbDC[1])) > 2:
            #             listDC.append(cellule[:NbDC[0]])

            for i, k in enumerate(NbDC):
                newCellule = cellule[k + 1:]
                if newCellule.find(sep) != -1:
                    listDC.append(newCellule[:newCellule.index(sep)])
                else:
                    listDC.append(newCellule)

        else:
            # prefix = cellule[:NbDC[0]-1]
            listDC.append(cellule[:NbDC[0]])

            for k in NbDC:
                listDC.append(prefix + cellule[k + 1])
    else:
        if len(cellule[NbDC[0] + 1:]) > 1:
            listDC.append(cellule[NbDC[0] + 1:])
        else:
            listDC.append(prefix + cellule[NbDC[0] + 1])

    return listDC






# stat_maintenance ={
#     titre1:{
#         DC1:{
#             type1:['des1', 'des2'],
#             type2:['des1', 'des2'],
#         },
#         DC2:{
#             type1:['des1', 'des2'],
#             type2:['des1', 'des2'],
#         }
#     },
#     titre2:{
#         DC1:{
#             type1:['des1', 'des2'],
#             type2:['des1', 'des2'],
#         },
#         DC2:{
#             type1:['des1', 'des2'],
#             type2:['des1', 'des2'],
#         }
#     }
# }


def premierdegree(cellule):
    keys_list = list()

    if cellule.find("|") != -1:
        sep = "|"
    elif cellule.find("-") != -1:
        sep = "-"

    if cellule[0] != "[":
        keys_list.append(cellule[0:cellule.index("]")].upper())
    else:
        keys_list.append(cellule[cellule.index("[") + 1:cellule.index("]")].upper())

    keys_list.append(cellule[cellule.index("]") + 1:cellule.index(sep)].lstrip().rstrip().replace(" ", "_").upper())
    keys_list.append(cellule[cellule.index(sep) + 1:].lstrip().rstrip().replace(" ", "_").upper())

    return keys_list


def deuxiemedegree(string):
    DC_list = list()

    if string.find("&") != -1:
        DC_list.append(string[0:string.index("&")])
        DC_list.append(string[0:string.index("&") - 1] + string[string.index("&") + 1:])
    else:

        if string.find("_") != -1:
            sep = "_"
        elif string.find("/") != -1:
            sep = "/"

        list_word = string.split(sep)
        for DC in list_word:
            if (len(DC) != 0):
                if (DC != "CORE") and (DC != "ALL") and (DC != "-") and (DC != "POP") and (DC != "DC") and (
                        DC != "DC") and (DC != "BACKBONE") and (DC != "DU"):
                    filterspecialChar = re.sub(r"[^a-zA-Z0-9]", "", DC)
                    if len(filterspecialChar) != 0:
                        DC_list.append(filterspecialChar)

    return DC_list



def DC_extrators(Data_OVH_events):
    stat_maintenance = dict()
    ii = 0

    for index, row in Data_OVH_events.iterrows():
        cellule = row["name"]

        start_indices = [j.start() for j in re.finditer("\[", row["name"])]
        end_indices = [k.start() for k in re.finditer("\]", row["name"])]

        if (len(start_indices) != len(end_indices)) or (len(start_indices) == 1):


            if (cellule.find("|") != -1) or (cellule.find("-") != -1):
                keys_list = premierdegree(cellule)

                if (keys_list[1].find("/") != -1) or (keys_list[1].find("-") != -1):
                    listDC = extractPrefix(keys_list[1])
                else:
                    if (keys_list[1].find("&") != -1) or (keys_list[1].find("_") != -1):

                        listDC = deuxiemedegree(keys_list[1])
                    else:
                        listDC = [keys_list[1]]

                for DC in listDC:
                    try:

                        if DC in stat_maintenance[keys_list[0]]:
                            if keys_list[2] in stat_maintenance[keys_list[0]][DC]:
                                stat_maintenance[keys_list[0]][DC][keys_list[2]].append(cellule)
                            else:
                                stat_maintenance[keys_list[0]][DC][keys_list[2]] = list()
                                stat_maintenance[keys_list[0]][DC][keys_list[2]].append(cellule)
                            print('FORMAT OK =', cellule)
                        else:
                            stat_maintenance[keys_list[0]][DC] = dict()
                            stat_maintenance[keys_list[0]][DC][keys_list[2]] = list()
                            stat_maintenance[keys_list[0]][DC][keys_list[2]].append(cellule)
                            print('FORMAT OK =', cellule)
                    except:
                        # print('index', index)
                        print("FORMAT TEXT ERROR = ", cellule)

            else:
                print('FORMAT TEXT ERROR =', cellule)


        else:
            ii = ii + 1
            keys_list = list()
            # cellule = row["name"]
            #
            # start_indices = [j.start() for j in re.finditer("\[", cellule)]
            # end_indices = [k.start() for k in re.finditer("\]", cellule)]

            for ei, si in enumerate(start_indices):
                keys_worl = cellule[si + 1:end_indices[ei]]
                keys_worl = keys_worl.lstrip()
                keys_worl = keys_worl.rstrip()
                keys_list.append(keys_worl.replace(" ", "_").upper())

            if len(start_indices) == 3:
                for i in range(0, 4):
                    if keys_list[0] in stat_maintenance:

                        if (keys_list[1].find("/") != -1) or (keys_list[1].find("-") != -1):
                            listDC = extractPrefix(keys_list[1])
                        else:
                            if (keys_list[1].find("&") != -1) or (keys_list[1].find("_") != -1):

                                listDC = deuxiemedegree(keys_list[1])
                            else:
                                listDC = [keys_list[1]]

                        for DC in listDC:

                            if DC in stat_maintenance[keys_list[0]]:
                                if keys_list[2] in stat_maintenance[keys_list[0]][DC]:
                                    stat_maintenance[keys_list[0]][DC][keys_list[2]].append(cellule[end_indices[-1] + 1:])
                                else:
                                    stat_maintenance[keys_list[0]][DC][keys_list[2]] = list()
                            else:
                                stat_maintenance[keys_list[0]][DC] = dict()
                    else:
                        stat_maintenance[keys_list[0]] = dict()
            else:
                for i in range(0, 3):

                    if len(keys_list) != 0:
                        if (keys_list[0].find("/") != -1) or (keys_list[0].find("-") != -1):

                            listDC = extractPrefix(keys_list[0])
                        else:
                            if (keys_list[0].find("&") != -1) or (keys_list[0].find("_") != -1):
                                # print('mot contenant des caractere 2 =',keys_list[0])
                                listDC = deuxiemedegree(keys_list[0])
                            else:
                                listDC = [keys_list[0]]

                        for DC in listDC:

                            if DC in stat_maintenance['SCHEDULED']:

                                if keys_list[1] in stat_maintenance['SCHEDULED'][DC]:

                                    try:
                                        # stat_maintenance[keys_list[0]][DC][next(iter(stat_maintenance[keys_list[0]][DC]))].append(cellule[end_indices[-1]+1 :])
                                        stat_maintenance['SCHEDULED'][DC][keys_list[1]].append(
                                            cellule[end_indices[-1] + 1:])
                                        print('FORMAT OK =', cellule)

                                    except:
                                        print('FORMAT TEXT ERROR 2 =', cellule)

                                else:
                                    stat_maintenance['SCHEDULED'][DC][keys_list[1]] = list()

                            else:
                                stat_maintenance['SCHEDULED'][DC] = dict()

    print('END DC EXTRACTOR ','-'*50)
    return stat_maintenance


def compte_le_mombre_de_maint_par_DC(DC_dict):
    New_dict = dict()
    for keys, value in DC_dict.items():
        New_dict[keys] = len(value)

    sorted_New_dict = OrderedDict(sorted(New_dict.items(), key=lambda x: x[1], reverse=True))

    return list(sorted_New_dict.keys()), list(sorted_New_dict.values())


def create_bar_graphe(x, y, xlabel, ylabel, title):
    def addtext(x, y, z):
        for i in range(len(x)):
            plt.text(i, y[i] // 2, z[i], ha='center', va='bottom', rotation=0,
                     bbox=dict(facecolor='pink', alpha=0.8))


    plt.figure(figsize=(20, 10))
    plt.bar(x, y)
    plt.xticks(rotation=90, ha='right')

    # Call function
    addtext(x, y, y)

    # Define labels
    plt.title(title)
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)

    # Display plot
    # plt.savefig("../../images/type_de_maintenance.png")
    plt.show()

def computeOcurenceOfWeekday(name_):

    all_weekday = []
    dictWeekday = dict()

    for i in findAllIndexKeyWord(name_):
        date = timesPubli[i].replace(" UTC", "")
        all_weekday.append(findDay(date))

    for day in list(set(all_weekday)):
        dictWeekday[day] = all_weekday.count(day)

    sortedWeekday = OrderedDict(sorted(dictWeekday.items(), key=lambda x: x[1],reverse=True))

    return sortedWeekday


def fonc(stat_maintenance):

    xx = list()
    yy = list()

    xxx = list()
    yyy = list()

    for keys, value in stat_maintenance['SCHEDULED'].items():

        if (len(value) != 1) and (len(value) != 0):

            ment_total = 0
            try:
                x1 = list()
                labels = list()

                for ke, va in value.items():
                    ment_total = len(va) + ment_total
                    labels.append(ke)
                    x1.append(len(va))

                    subManibtenanceWeekday = computeOcurenceOfWeekday(ke)
                    #print(list(subManibtenanceWeekday.keys()))
                    print(list(subManibtenanceWeekday.values()))
                    fig2, ax2 = plt.subplots()
                    ax2.pie(list(subManibtenanceWeekday.values()), labels=list(subManibtenanceWeekday.keys()),
                            autopct='%1.1f%%')
                    ax2.set_title("maintenances de " + ke + ' sur ' + keys)
                    plt.show()

                    create_bar_graphe(list(subManibtenanceWeekday.keys()), list(subManibtenanceWeekday.values()),
                                      'Date de maintenance', 'Niombre par jour', 'Repartition des maintenances par jour de '+ke+' de '+keys)

            except:
                print("ERROR TYPE SCHEDULED  = ", ke)

            xx.append(keys)
            yy.append(ment_total)


        else:
            ment_total2 = 0
            try:
                for ke, va in value.items():
                    ment_total2 = len(va) + ment_total2
            except:
                print("ERROR =", keys, value)

            xxx.append(keys)
            yyy.append(ment_total2)

    return xx, yy




if __name__ == '__main__':
    pages = [2, 3, 4]
    data_OVHcloudNetworkStatus = web_scraping(pages)

    # Sauvegarde du dataset 'data_OVHcloudNetworkStatus.csv' en csv
    data_OVHcloudNetworkStatus.to_csv('../data/data_OVHcloudNetworkStatus.csv')
    DC_maintenance = DC_extrators(data_OVHcloudNetworkStatus)



    # Creation du graphe type d'impact des annonces
    x = list(dict(data_OVHcloudNetworkStatus['impact'].value_counts()).keys())
    y = list(dict(data_OVHcloudNetworkStatus['impact'].value_counts()).values())
    create_bar_graphe(x,y, 'type impact', 'Nombre de publication', 'type impact')

    xx, yy = fonc(DC_maintenance)

    print(xx)
    print(yy)

    dict_maintenance_DC = dict()
    for i, DC_ in enumerate(xx):
        dict_maintenance_DC[DC_] = yy[i]
    dict_maintenance_DC

    # Creation du graphe de la repartition des maintenance par DC
    sortedDict = OrderedDict(sorted(dict_maintenance_DC.items(), key=lambda x: x[1], reverse=True))
    x = list(sortedDict.keys())
    y = list(sortedDict.values())
    create_bar_graphe(x, y, 'type de la maintenance', 'Nombre de publication', 'Repartition des maintenances par datacenter')

    # Creation du graphe type de maintenance du datacenter 'GRA2'
    x, y = compte_le_mombre_de_maint_par_DC(DC_maintenance['SCHEDULED']['GRA2'])
    create_bar_graphe(x, y, 'type de la maintenance', 'Nombre de publication', 'type de la maintenance sur GRA2 ')








